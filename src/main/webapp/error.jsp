<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ include file="WEB-INF/jsp/fragment/Variables.jspf" %>

<c:set var="page" value="error.jsp" scope="session"/>

<html lang="${language}">
<head>
    <title>Error Page</title>
    <link rel="stylesheet" href="css/error.css">
    <link rel="stylesheet" href="css/header_footer.css">
</head>
<body>
<div class="main_container">
    <jsp:include page="WEB-INF/jsp/common_header.jsp"/>
    <div class="container">
      <h1><fmt:message key="${errorMsg}"/></h1>
    </div>
    <div>
        <jsp:include page="html/footer.html"/>
    </div>
</div>
</body>
</html>

