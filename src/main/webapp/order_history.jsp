<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ include file="WEB-INF/jsp/fragment/Variables.jspf" %>

<c:set var="page" value="Servlet?command=next&pageNum=${currentPage}" scope="session"/>
<jsp:include page="WEB-INF/jsp/roleChecker.jsp"/>

<html>
<head>
    <title>Order history</title>
    <link rel="stylesheet" href="css/user_page.css">
    <link rel="stylesheet" href="css/header_footer.css">
</head>
<body>
<div class="main_container">
    <jsp:include page="WEB-INF/jsp/common_header.jsp"/>
    <div style="height: 85%" class="inner_container">
        <div class="my_account"><h1><fmt:message key="header.orderHistory"/></h1><br/>
            <c:if test="${message ne null}">
                <h2><fmt:message key="${message}"/></h2>
            </c:if>
            <hr/>
        </div>
        <div class="container">
            <table>
                <tr>
                    <th class="order_th">
                        <fmt:message key="header.orderNumber"/>
                    </th>
                    <th class="order_th">
                        <fmt:message key="header.date"/>
                    </th>
                    <th class="order_th">
                        <fmt:message key="header.tradeName"/>
                    </th>
                    <th class="order_th">
                        <fmt:message key="header.amount"/>
                    </th>
                    <th class="order_th">
                        <fmt:message key="header.totalPrice"/>
                    </th>
                    <th class="order_th">
                        <fmt:message key="header.state"/>
                    </th>
                </tr>
                <c:set var="last" value="${(currentPage - 1) * 10}"/>
                <c:forEach items="${sessionScope.user.orderDto}" var="orderDto" begin="${last}" end="${last+10}">
                    <tr>
                        <td align="center">${orderDto.number}</td>
                        <td align="center">${orderDto.date}</td>
                        <td align="center">${orderDto.tradeName}</td>
                        <td align="center">${orderDto.amount}</td>
                        <td align="center">${orderDto.totalPrice}</td>
                        <c:set var="description" value="order.isOpen.${orderDto.isOpen}"/>
                        <td align="center"><fmt:message key="${description}"/></td>
                    </tr>
                </c:forEach>
                <tr>
                    <td colspan="3" align="center">
                        <c:if test="${last gt 0}">
                            <form action="Servlet" method="post">
                                <input class="next_button" type="submit" value="<fmt:message key="action.previous"/>">
                                <input type="hidden" value="next" name="command"/>
                                <input type="hidden" value="${currentPage - 1}" name="pageNum"/>
                            </form>
                        </c:if>
                    </td>
                    <td colspan="3" align="center">
                        <form action="Servlet" method="post">
                            <input class="next_button" type="submit" value="<fmt:message key="action.next"/>">
                            <input type="hidden" value="${currentPage + 1}" name="pageNum"/>
                            <input type="hidden" value="next" name="command">
                        </form>
                    </td>
                </tr>
            </table>
        </div>

    </div>
</div>

<div>
    <jsp:include page="html/footer.html"/>
</div>
</body>
</html>
