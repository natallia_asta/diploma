<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ include file="WEB-INF/jsp/fragment/Variables.jspf" %>

<c:set var="page" value="user_page.jsp" scope="session"/>
<jsp:include page="WEB-INF/jsp/roleChecker.jsp"/>

<html>
<head>
    <title>User page</title>
    <link rel="stylesheet" href="css/user_page.css">
    <link rel="stylesheet" href="css/header_footer.css">
</head>
<body>
<div class="main_container">
    <jsp:include page="WEB-INF/jsp/common_header.jsp"/>
    <div style="height: 85%" class="inner_container">
        <div class="my_account"><h1><fmt:message key="header.myAccount"/></h1><br/>
            <div class="message">
                <h2>
                    <c:if test="${sessionScope.message ne null}">
                        <fmt:message key="${sessionScope.message}"/>
                    </c:if>
                    <c:if test="${sessionScope.registeredNumber ne null}">
                        <c:out value="${sessionScope.registeredNumber}"/>
                    </c:if>
                </h2>
                <c:remove var="message" scope="session"/>
                <c:remove var="registeredNumber" scope="session"/>
                <c:if test="${message ne null}">
                    <h2><fmt:message key="${message}"/></h2>
                </c:if>
            </div>
            <hr/>
        </div>
        <div class="under_line">
            <div class="left_container">
                <div class="left_menu">
                    <form class="white_form" action="Servlet" method="post">
                        <input class="button" type="submit" value="<fmt:message key="action.orderHistory"/>"/>
                        <input type="hidden" value="orderHistory" name="command"/>
                    </form>
                </div>
                <div class="left_menu">
                    <form action="Servlet" method="post">
                        <input class="button" type="submit" value="<fmt:message key="header.myRecipes"/>"/>
                        <input type="hidden" value="myRecipes" name="command"/>
                    </form>
                </div>
                <div class="left_menu">
                    <form action="Servlet" method="post">
                        <input class="button" type="submit" value="<fmt:message key="header.myInquiries"/>"/>
                        <input type="hidden" value="myInquiries" name="command"/>
                    </form>
                </div>
            </div>

            <div id="details"><fmt:message key="header.accountDetails"/>
                <br/><br/>
                <p>${sessionScope.user.getName()}<p>
                <p>${sessionScope.user.getSurname()}<p>
                <p>${sessionScope.user.getTelephone()}<p>
            </div>
        </div>
    </div>
</div>
<div>
    <jsp:include page="html/footer.html"/>
</div>
</body>
</html>