<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" language="java" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ include file="WEB-INF/jsp/fragment/Variables.jspf" %>

<c:set var="page" value="search.jsp" scope="session"/>
<jsp:include page="WEB-INF/jsp/roleChecker.jsp"/>

<html>
<head>
    <title>Search</title>
    <link rel="stylesheet" href="css/search.css"/>
    <link rel="stylesheet" href="css/header_footer.css"/>
</head>
<body>

<div class="main_container">
    <jsp:include page="WEB-INF/jsp/common_header.jsp"/>
    <div class="container">
        <form action="Servlet" method="post">
            <div id="search"><fmt:message key="header.SEARCH"/></div>
            <div><input class="search_field" type="search" name="search" value=""
                        placeholder="<fmt:message key="text.paracetamol"/>"/>
                <input class="search_button" type="submit" value="<fmt:message key="action.search"/>"/>
                <input type="hidden" value="search" name="command"/>
            </div>
            <c:if test="${message ne null}">
                <div class="errorMessage">
                    <h3><fmt:message key="${message}"/></h3>
                </div>
            </c:if>
        </form>
    </div>
    <div>
        <jsp:include page="html/footer.html"/>
    </div>
</div>
</body>
</html>
