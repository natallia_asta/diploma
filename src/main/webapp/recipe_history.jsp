<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ include file="WEB-INF/jsp/fragment/Variables.jspf" %>

<c:set var="page" value="recipe_history.jsp" scope="session"/>
<jsp:include page="WEB-INF/jsp/roleChecker.jsp"/>

<html>
<head>
    <title>Order history</title>
    <link rel="stylesheet" href="css/user_page.css">
    <link rel="stylesheet" href="css/header_footer.css">
</head>
<body>
<div class="main_container">
    <jsp:include page="WEB-INF/jsp/common_header.jsp"/>
    <div style="height: 85%" class="inner_container">
        <div class="my_account"><h1><fmt:message key="header.myRecipes"/></h1><br/>
            <h2><c:out value="${message}"/></h2>
            <hr/>
        </div>
        <div class="container">
            <fmt:message key="header.myRecipes"/>
            <table>
                <tr>
                    <th>
                        <fmt:message key="header.recipeNumber"/>
                    </th>
                    <th>
                        <fmt:message key="header.tradeName"/>
                    </th>
                    <th>
                        <fmt:message key="header.amount"/>
                    </th>
                    <th>
                        <fmt:message key="header.validityDate"/>
                    </th>
                    <th>
                        <fmt:message key="header.notRedeemed"/>
                    </th>
                    <th>
                        <fmt:message key="header.state"/>
                    </th>
                </tr>
                <c:forEach items="${sessionScope.user.recipeDto}" var="recipeDto">
                    <tr>
                        <td align="center">${recipeDto.recipeNumber}</td>
                        <td align="center">${recipeDto.tradeName}</td>
                        <td align="center">${recipeDto.amount}</td>
                        <td align="center">${recipeDto.validityDate}</td>
                        <td align="center">${recipeDto.notRedeemed}</td>
                        <c:set var="description" value="${recipeDto.recipeStatus.description}"/>
                        <td align="center"><fmt:message key="${description}"/></td>
                    </tr>
                </c:forEach>
            </table>
        </div>
        <br/>
        <div id="a">
            <a class="on_page" href="write_doctor.jsp"><b><fmt:message key="link.writeToDoctor"/></b></a>
        </div>
    </div>
</div>

<div>
    <jsp:include page="html/footer.html"/>
</div>
</body>
</html>

