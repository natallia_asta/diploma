<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ include file="WEB-INF/jsp/fragment/Variables.jspf" %>

<c:set var="page" value="pharmacist_page.jsp" scope="session"/>
<c:set var="role" value="${sessionScope.user.userRole}"/>
<c:if test="${role ne 'PHARMACIST'}">
    <jsp:forward page="index.jsp"/>
</c:if>

<html>
<head>
    <title>Pharmacist</title>
    <link rel="stylesheet" href="css/user_page.css"/>
    <link rel="stylesheet" href="css/header_footer.css"/>
</head>

<body>
<div class="main_container">
    <jsp:include page="WEB-INF/jsp/common_header.jsp"/>
    <div style="height: 700px" class="inner_container">
        <form action="pharmacist" method="post">
            <div class="my_account"><h1><fmt:message key="header.pharmacistAccount"/></h1>
                <hr/>
                <div class="under_line">
                    <div><fmt:message key="header.medicineList"/>
                        <table>
                            <tr>
                                <th>N</th>
                                <th><fmt:message key="header.tradeName"/></th>
                                <th><fmt:message key="header.INN"/></th>
                                <th><fmt:message key="header.doze"/></th>
                                <th><fmt:message key="header.form"/></th>
                                <th><fmt:message key="header.producer"/></th>
                                <th><fmt:message key="header.price"/></th>
                                <th><fmt:message key="header.amount"/></th>
                            </tr>
                            <c:forEach items="${requestScope.medicineList}" var="medicine">
                                <tr>
                                    <td>${medicine.id}</td>
                                    <td>${medicine.tradeName}</td>
                                    <td>${medicine.medicineInn}</td>
                                    <td>${medicine.doze}</td>
                                    <c:set var="form" value="${medicine.form.description}"/>
                                    <td><fmt:message key="${form}"/></td>
                                    <td>${medicine.producer}</td>
                                    <td>${medicine.price}</td>
                                    <td>${medicine.availableAmount}</td>
                                </tr>
                            </c:forEach>
                        </table>
                    </div>
                    <div><fmt:message key="header.accountDetails"/>
                        <br/><br/>
                        <p>${sessionScope.user.name}</p>
                        <p>${sessionScope.user.surname}</p>
                        <p>${sessionScope.user.telephone}</p>
                        <br/><br/>
                        <hr/>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <div>
        <jsp:include page="html/footer.html"/>
    </div>
</div>
</body>
</html>
