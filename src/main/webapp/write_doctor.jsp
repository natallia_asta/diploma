<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ include file="WEB-INF/jsp/fragment/Variables.jspf" %>

<c:set var="page" value="write_doctor.jsp" scope="session"/>


<html>
<head>
    <title>User page</title>
    <link rel="stylesheet" href="css/user_page.css">
    <link rel="stylesheet" href="css/registration.css">
    <link rel="stylesheet" href="css/header_footer.css">
</head>
<body>
<div class="main_container">
    <jsp:include page="WEB-INF/jsp/common_header.jsp"/>
    <div style="height: 700px" class="container">
        <form action="Servlet" method="post">
            <table>
                <tr>
                    <td><fmt:message key="header.recipeNumber"/></td>
                    <td><input class="send_text_field" type="text" name="number" required/></td>
                </tr>
                <tr>
                    <td><fmt:message key="header.forHowManyDays"/></td>
                    <td><input id="days"
                               title="<fmt:message key="text.toProlongRecipe"/>"
                               type="number"
                               min="1"
                               max="15"
                               name="days"
                               required/></td>
                </tr>
                <tr>
                    <td colspan="2"><input class="send_button"
                                           type="submit"
                                           value="<fmt:message key="action.send"/>"/></td>
                    <input type="hidden" value="writeToDoctor" name="command"/>
                </tr>
            </table>
            <div class="message">
                <c:if test="${message ne null}">
                    <b><fmt:message key="${message}"/></b>
                </c:if>
            </div>
            <div class="errorMessage">
                <c:if test="${errorMsg ne null}">
                    <b><fmt:message key="${errorMsg}"/></b>
                </c:if>
            </div>
        </form>
    </div>
    <div>
        <jsp:include page="html/footer.html"/>
    </div>
</div>
</body>
</html>

