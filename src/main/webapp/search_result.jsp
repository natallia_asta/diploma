<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ include file="WEB-INF/jsp/fragment/Variables.jspf" %>

<c:set var="page" value="search_result.jsp" scope="session"/>
<jsp:include page="WEB-INF/jsp/roleChecker.jsp"/>

<html>
<head>
    <title>Search result</title>
    <link rel="stylesheet" href="css/search_results.css"/>
    <link rel="stylesheet" href="css/header_footer.css"/>
</head>
<body>
<div class="main_container">
    <jsp:include page="WEB-INF/jsp/common_header.jsp"/>
    <form action="Servlet" method="post">
        <div class="container">
            <div class="table_name"><h1><fmt:message key="header.searchResult"/><br/></h1>
                <hr/>
            </div>
            <div class="table">
                <table>
                    <tr>
                        <th><fmt:message key="header.tradeName"/></th>
                        <th><fmt:message key="header.INN"/></th>
                        <th><fmt:message key="header.doze"/></th>
                        <th><fmt:message key="header.form"/></th>
                        <th><fmt:message key="header.producer"/></th>
                        <th><fmt:message key="header.price"/></th>
                        <th><fmt:message key="header.amount"/></th>
                        <th><fmt:message key="header.addToOrder"/></th>
                    </tr>
                    <c:set var="i" value="0"/>
                    <c:forEach items="${sessionScope.user.foundBySearch}" var="medicine">
                        <tr>
                            <td>${medicine.tradeName}</td>
                            <td>${medicine.medicineInn}</td>
                            <td align="center">${medicine.doze}</td>
                            <c:set var="description" value="${medicine.form.description}"/>
                            <td align="center"><fmt:message key="${description}"/></td>
                            <td>${medicine.producer}</td>
                            <td align="center">${medicine.price}</td>
                            <td align="center"><input class="number" type="number" min="0" value="1" name="amount"/>
                            </td>
                            <td align="center"><input class="check-box" type="checkbox" value="<c:out value="${i}"/>"
                                                      name="checkbox">
                            </td>
                            <c:set var="i" value="${i+1}"/>
                        </tr>
                        <c:if test="${medicine.isRecipeRequired()}">
                            <tr>
                                <td>&nbsp</td>
                            </tr>
                            <tr>
                                <td class="dark-red" colspan="3"><fmt:message key="search.addRecipeNumber"/>
                                </td>
                                <td><input type="text" value="" name="recipe_number"/></td>
                            </tr>
                        </c:if>
                    </c:forEach>
                    <tr>
                        <td colspan="7"></td>
                        <td align="center">
                            <input class="add_button" type="submit" value="<fmt:message key="action.add"/>"/>
                            <input type="hidden" value="addToOrder" name="command"/>
                        </td>
                    </tr>
                </table>
            </div>

            <c:if test="${message ne null}">
                <div class="message"><h3><fmt:message key="${message}"/></h3></div><br/>
            </c:if>
            <c:if test="${errorMsg ne null}">
                <div class="errorMessage"><h3><fmt:message key="${errorMsg}"/></h3></div><br/>
            </c:if>
            <div id="seeOrder"><a href="order.jsp"><fmt:message key="action.seeOrder"/></a></div>
        </div>
    </form>
    <div>
        <jsp:include page="html/footer.html"/>
    </div>
</div>
</body>
</html>
