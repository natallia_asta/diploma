<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ include file="WEB-INF/jsp/fragment/Variables.jspf" %>

<c:set var="page" value="doctor_page.jsp" scope="session"/>
<c:set var="role" value="${sessionScope.user.userRole}"/>
<c:if test="${role ne 'DOCTOR'}">
    <jsp:forward page="index.jsp"/>
</c:if>

<html lang="${language}">
<head>
    <title>User page</title>
    <link rel="stylesheet" href="css/user_page.css" />
    <link rel="stylesheet" href="css/header_footer.css"/>
</head>

<body>
<div style="height: 90%" class="main_container">
    <jsp:include page="WEB-INF/jsp/common_header.jsp"/>
    <div style="height: 85%" class="inner_container">
        <form action="login" method="post">
            <div class="my_account"><h1><fmt:message key="header.doctorAccount"/></h1>
                <hr/>
                <div class="under_line">
                    <div><fmt:message key="header.myRecipes"/></div>
                    <div class="container">
                        <fmt:message key="header.myInquiries"/>
                        <table>
                            <tr>
                                <th>
                                    <fmt:message key="header.recipeId"/>
                                </th>
                                <th>
                                    <fmt:message key="header.tradeName"/>
                                </th>
                                <th>
                                    <fmt:message key="header.amount"/>
                                </th>
                                <th>
                                    <fmt:message key="header.notRedeemed"/>
                                </th>
                                <th>
                                    <fmt:message key="header.forHowManyDays"/>
                                </th>
                                <th>
                                    <fmt:message key="header.inquiryStatus"/>
                                </th>
                            </tr>
                            <c:forEach items="${sessionScope.user.inquiryDto}" var="inquiryDto">
                                <tr>
                                    <c:set var="status" value="${inquiryDto.getInquiryStatus()}"/>
                                    <td align="center">${inquiryDto.getRecipeId()}</td>
                                    <td align="center">${inquiryDto.getTradeName()}</td>
                                    <td align="center">${inquiryDto.getAmount()}</td>
                                    <td align="center">${inquiryDto.getNotRedeemed()}</td>
                                    <td align="center">${inquiryDto.getDays()}</td>
                                    <c:set var="description" value="${inquiryDto.inquiryStatus.description}" />
                                    <td align="center"><fmt:message key="${description}"/></td>
                                </tr>
                            </c:forEach>
                        </table>
                    </div>
                    <div><fmt:message key="header.accountDetails"/>
                        <br/><br/>
                        <p>${sessionScope.user.name}<p>
                        <p>${sessionScope.user.surname}<p>
                        <p>${sessionScope.user.telephone}<p>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <div><jsp:include page="html/footer.html"/></div>
</div>
</body>
</html>
