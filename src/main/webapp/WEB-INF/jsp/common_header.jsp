<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<c:set var="isLoggedIn" value="${sessionScope != null && sessionScope.containsKey(\"user\")}"/>
<c:set var="role" value="${sessionScope.user.userRole}"/>
<fmt:setLocale value="${language}"/>
<fmt:setBundle basename="text"/>


<div class="header_container">
    <div class="inner_container"><span><b>pharmacy.by</b></span></div>
    <div class="inner_container">
        <ul>
            <li><a href="index.jsp"><fmt:message key="link.home"/></a></li>
            <li><a href="html/about.html"><fmt:message key="link.about"/></a></li>
            <li><a href="html/contacts.html"><fmt:message key="link.contacts"/></a></li>
        </ul>
    </div>
    <c:if test="${isLoggedIn && role eq 'CLIENT'}">
        <div class="inner_container"><a class="search" href="search.jsp"><fmt:message key="link.SEARCH"/></a></div>
    </c:if>
    <div class="inner_container">
        <form action="changeLanguage" method="post">
            <select id="language" name="language" onchange="submit()">
                <option value="en" ${language == 'en' ? 'selected' : ''}>English</option>
                <option value="ru" ${language == 'ru' ? 'selected' : ''}>Русский</option>
            </select>
            <input type="hidden" value="changeLanguage" name="command">
        </form>
    </div>
    <div class="inner_container">
        <ul>
            <c:choose>
                <c:when test="${isLoggedIn}">
                    <li>
                        <form action="Servlet" method="post">
                            <input class="logout_button" type="submit" value="<fmt:message key="action.logOut"/>"/>
                            <input type="hidden" value="logOut" name="command"/>
                        </form>
                    </li>
                    <li><a href="user_page.jsp"><fmt:message key="link.myAccount"/></a></li>
                </c:when>
                <c:otherwise>
                    <li><a href="registration.jsp"><fmt:message key="link.createAccount"/></a></li>
                </c:otherwise>
            </c:choose>
        </ul>
    </div>
</div>

