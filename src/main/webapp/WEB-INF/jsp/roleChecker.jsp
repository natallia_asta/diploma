<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:set var="role" value="${sessionScope.user.userRole}"/>
<c:if test="${role ne 'CLIENT'}">
    <jsp:forward page="../../index.jsp"/>
</c:if>