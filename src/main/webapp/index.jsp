<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ include file="WEB-INF/jsp/fragment/Variables.jspf" %>

<c:set var="page" value="index.jsp" scope="session"/>

<html>
<head>
    <title>Pharmacy</title>
    <link rel="stylesheet" href="css/index.css"/>
    <link rel="stylesheet" href="css/header_footer.css"/>
</head>

<body>
<div class="main_container">
    <jsp:include page="WEB-INF/jsp/common_header.jsp"/>
    <c:choose>
        <c:when test="${sessionScope.user == null}">
            <form action="login" method="post">
                <div class="container">
                    <!-- login -->
                    <div data-label="login">
                        <p><input class="text_field" type="text" name="login"
                                  value=""
                                  placeholder="<fmt:message key="login.userName"/>"/>
                        </p>
                    </div>
                    <!-- password -->
                    <div data-label="password">
                        <p><input class="text_field"
                                  type="password"
                                  name="password"
                                  value=""
                                  placeholder="<fmt:message key="login.password"/>"/>
                        </p>
                    </div>
                    <!-- submit button -->
                    <div>
                        <input class="login_button" type="submit" value="<fmt:message key="action.logIn"/>"/><br/>
                        <input type="hidden" value="logIn" name="command"/>
                    </div>
                    <div class="errorMessage">
                        <c:if test="${errorMsg ne null}">
                            <b><fmt:message key="${errorMsg}"/></b>
                        </c:if>
                    </div>
                    <div class="message">
                        <c:if test="${inviteMessage ne null}">
                            <b><fmt:message key="${inviteMessage}"/></b>
                        </c:if>
                    </div>
                </div>
            </form>
        </c:when>
        <c:otherwise>
            <div class="home-container">
                <div id="welcome"><b>Welcome to the pharmacy</b></div>
            </div>
        </c:otherwise>
    </c:choose>
    <jsp:include page="html/footer.html"/>
</div>
</body>
</html>

