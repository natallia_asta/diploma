<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ include file="WEB-INF/jsp/fragment/Variables.jspf" %>

<c:set var="page" value="order.jsp" scope="session"/>
<jsp:include page="WEB-INF/jsp/roleChecker.jsp"/>

<html>
<head>
    <title>Order</title>
    <link rel="stylesheet" href="css/order.css"/>
    <link rel="stylesheet" href="css/header_footer.css"/>
</head>
<body>
<div class="main_container">
    <jsp:include page="WEB-INF/jsp/common_header.jsp"/>
    <div class="container">
        <form action="Servlet" method="post">
            <div class="table_name"><h1><fmt:message key="header.myOrder"/></h1></div>
            <hr/>
            <table class="result-table">
                <thead>
                <tr>
                    <th><fmt:message key="header.tradeName"/>
                    </th>
                    <th><fmt:message key="header.form"/>
                    </th>
                    <th><fmt:message key="header.producer"/>
                    </th>
                    <th><fmt:message key="header.amount"/>
                    </th>
                    <th><fmt:message key="header.price"/>
                    </th>
                    <th><fmt:message key="header.deleteFromOrder"/>
                    </th>
                </tr>
                </thead>
                <tbody>
                <c:set var="i" value="0"/>
                <c:forEach items="${sessionScope.user.order.medicineList}" var="medicine">
                    <tr>
                        <td>${medicine.tradeName}</td>
                        <td align="center">${medicine.form}</td>
                        <td>${medicine.producer}</td>
                        <td align="center">${medicine.amount}</td>
                        <td align="center">${medicine.price}</td>
                        <td align="center"><input class="check-box" type="checkbox" name="checkbox" value="${i}"/></td>
                        <c:set var="i" value="${i+1}"/>
                    </tr>
                </c:forEach>
                <tr>
                    <td colspan="5"></td>
                    <td align="center">
                        <input class="delete_button" type="submit" value="<fmt:message key="action.delete"/>"/>
                        <input type="hidden" value="delete" name="command"/>
                    </td>
                </tr>
                <tr>
                    <td><b><fmt:message key="header.totalPrice"/></b></td>
                    <td align="center"><b><c:out value="${sessionScope.user.order.totalPrice}"/></b></td>
                </tr>
                </tbody>
            </table>
        </form>
        <div>
            <form action="Servlet" method="post">
                <br/><br/>
                <div class="under">
                    <div class="errorMessage">
                        <c:if test="${errorMsg ne null}">
                            <b>
                                <fmt:message key="${errorMsg}"/>
                            </b>
                        </c:if>
                    </div>
                </div>
                <br/>
                <div class="under">
                    <input class="confirm_button" type="submit" value="<fmt:message key="action.confirmOrder"/>"/>
                    <input type="hidden" value="confirmOrder" name="command"/>
                </div>
            </form>
        </div>
    </div>
    <div>
        <jsp:include page="html/footer.html"/>
    </div>
</div>
</body>
</html>
