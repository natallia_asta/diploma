﻿<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" language="java" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ include file="WEB-INF/jsp/fragment/Variables.jspf" %>

<c:set var="page" value="registration.jsp" scope="session"/>

<!DOCTYPE html>
<html>
<head>
    <title>Registration</title>
    <link rel="stylesheet" href="css/registration.css"/>
    <link rel="stylesheet" href="css/header_footer.css"/>
</head>
<body>
<div class="main_container">
    <jsp:include page="WEB-INF/jsp/common_header.jsp"/>
    <form action="registration" method="post" accept-charset="UTF-8">
        <div class="container">
            <div>

                <c:if test="${errorMsg != null}">
                    <h3><fmt:message key="${errorMsg}"/></h3>
                </c:if>

                <table>
                    <tr>
                        <td><fmt:message key="registration.name"/><span>*</span></td>
                        <td><input class="text_field"
                                   type="text"
                                   name="name"
                                   value="${userData.name}"
                                   required/></td>
                    </tr>

                    <tr>
                        <td><fmt:message key="registration.surname"/><span>*</span></td>
                        <td><input class="text_field"
                                   type="text"
                                   name="surname"
                                   value="${userData.surname}"
                                   required/></td>
                    </tr>

                    <tr>
                        <td><fmt:message key="registration.userName"/><span>*</span></td>
                        <td><input class="text_field"
                                   type="text"
                                   name="login"
                                   value="${userData.login}"/></td>
                    </tr>

                    <tr>
                        <td><fmt:message key="registration.password"/><span>*</span></td>
                        <td><input type="password"
                                   pattern=".{3,}"
                                   required title="3 symbol minimum "
                                   name="password"
                                   value=""
                                   class="text_field"/></td>
                    </tr>

                    <tr>
                        <td><fmt:message key="registration.password2"/><span>*</span></td>
                        <td><input class="text_field"
                                   type="password"
                                   name="password_repeat"
                                   value=""
                                   required/></td>
                    </tr>

                    <tr>
                        <td><fmt:message key="registration.phone"/><span>*</span></td>
                        <td><input type="tel" name="telephone" class="text_field"
                                   placeholder="+375(29)666-77-88"
                                   pattern="+[0-9]{3}([0-9]{2})[0-9]{3}-[0-9]{2}-[0-9]{2}"
                                   value="${userData.telephone}"
                                   required/></td>
                    </tr>

                    <tr>
                        <td align="center" colspan="2"><input class="button"
                                                              type="submit"
                                                              value="<fmt:message key="action.registration"/>"/>
                            <input type="hidden" value="toRegister" name="command"/>
                        </td>
                    </tr>

                </table>
            </div>
        </div>
    </form>
    <div>
        <jsp:include page="html/footer.html"/>
    </div>
</div>
</body>
</html>
