<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ include file="WEB-INF/jsp/fragment/Variables.jspf" %>

<c:set var="page" value="inquiry_history.jsp" scope="session"/>
<jsp:include page="WEB-INF/jsp/roleChecker.jsp"/>

<html>
<head>
    <title>Inquiry history</title>
    <link rel="stylesheet" href="css/user_page.css">
    <link rel="stylesheet" href="css/header_footer.css">
</head>
<body>
<div class="main_container">
    <jsp:include page="WEB-INF/jsp/common_header.jsp"/>
    <div style="height: 85%" class="inner_container">
        <div class="my_account"><h1><fmt:message key="header.myInquiries"/></h1><br/>
            <h2><c:out value="${message}"/></h2>
            <hr/>
        </div>
        <div class="container">
            <table>
                <tr>
                    <th>
                        <fmt:message key="header.inquiryNumber"/>
                    </th>
                    <th>
                        <fmt:message key="header.recipeNumber"/>
                    </th>
                    <th>
                        <fmt:message key="header.tradeName"/>
                    </th>
                    <th>
                        <fmt:message key="header.amount"/>
                    </th>
                    <th>
                        <fmt:message key="header.validityDate"/>
                    </th>
                    <th>
                        <fmt:message key="header.notRedeemed"/>
                    </th>
                    <th>
                        <fmt:message key="header.inquiryStatus"/>
                    </th>
                </tr>
                <c:forEach items="${sessionScope.user.inquiryDto}" var="inquiryDto">
                    <tr>
                        <td align="center">${inquiryDto.id}</td>
                        <td align="center">${inquiryDto.number}</td>
                        <td align="center">${inquiryDto.tradeName}</td>
                        <td align="center">${inquiryDto.amount}</td>
                        <td align="center">${inquiryDto.validityDate}</td>
                        <td align="center">${inquiryDto.notRedeemed}</td>
                        <c:set var="description" value="${inquiryDto.inquiryStatus.description}"/>
                        <td align="center"><fmt:message key="${description}"/></td>
                    </tr>
                </c:forEach>
            </table>
        </div>
        <br/>
    </div>
</div>

<div>
    <jsp:include page="html/footer.html"/>
</div>
</body>
</html>


