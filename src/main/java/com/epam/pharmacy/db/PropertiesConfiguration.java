package com.epam.pharmacy.db;

import com.epam.pharmacy.exceptions.DaoException;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Properties;

/**
 * Extracts properties values for connection to database from file.
 */
public class PropertiesConfiguration {

    private static final String FILE_NAME = "database.properties";
    private static final String ERROR_MESSAGE = "Can't open database properties file";
    private static final String DB_DRIVER = "db.driver";
    private static final String DB_URL = "db.url";
    private static final String DB_USER = "db.user";
    private static final String DB_PASSWORD = "db.password";
    private static final String DB_MAX_ACTIVE = "db.maxActive";

    private String driver;
    private String url;
    private String user;
    private String password;
    private String maxActive;
    /**
     * Creates an instance of the class by loading property file and establishing
     * the values of private fields of this class.
     *
     * @throws DaoException if the property file not found
     */
    public PropertiesConfiguration () throws DaoException {
        Properties properties = new Properties();
        URL path = this.getClass().getClassLoader().getResource(FILE_NAME);
        try {
            File file = new File(path.toURI());
            properties.load(new FileReader(file));
        } catch (IOException | URISyntaxException e) {
            throw new DaoException(ERROR_MESSAGE + e);
        }
        driver = properties.getProperty(DB_DRIVER);
        url = properties.getProperty(DB_URL);
        user = properties.getProperty(DB_USER);
        password = properties.getProperty(DB_PASSWORD);
        maxActive = properties.getProperty(DB_MAX_ACTIVE);
    }

    /**
     * Getter for the private String driver variable.
     *
     * @return the established in constructor value of the field
     */
    public String getDriver() {
        return driver;
    }
    /**
     * Getter for the private String url variable.
     *
     * @return the established in constructor value of the field
     */
    public String getUrl() {
        return url;
    }
    /**
     * Getter for the private String user variable.
     *
     * @return the established in constructor value of the field
     */
    public String getUser() {
        return user;
    }
    /**
     * Getter for the private String password variable.
     *
     * @return the established in constructor value of the field
     */
    public String getPassword() {
        return password;
    }
    /**
     * Getter for the private String maxActive variable.
     *
     * @return the established in constructor value of the field
     */
    public String getMaxActive() {
        return maxActive;
    }

}
