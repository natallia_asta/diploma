package com.epam.pharmacy.db;

import com.epam.pharmacy.exceptions.DaoException;

import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.Semaphore;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class ConnectionPool {

    private static final Queue<Connection> pool = new LinkedList<>();
    private static final Lock INSTANCE_LOCK = new ReentrantLock();
    private static final String ERROR_WHILE_GETTING_A_CONNECTION = "Error while getting a connection ";

    private final Lock poolLock = new ReentrantLock();
    private static Semaphore semaphore;
    private static ConnectionPool instance;


    public static ConnectionPool getInstance() throws DaoException {
        if (instance == null) {
            INSTANCE_LOCK.lock();
            try {
                if (instance == null) {
                    instance = new ConnectionPool();
                }
            } finally {
                INSTANCE_LOCK.unlock();
            }
        }
        return instance;
    }

    private ConnectionPool() throws DaoException {
        PropertiesConfiguration configuration = new PropertiesConfiguration();
        int maxActive = Integer.parseInt(configuration.getMaxActive());
        try {
            semaphore = new Semaphore(maxActive, true);
            Class.forName(configuration.getDriver());
            initializeConnectionPool(configuration);
        } catch (SQLException | ClassNotFoundException e) {
            throw new DaoException(ERROR_WHILE_GETTING_A_CONNECTION, e);
        }
    }

    private void initializeConnectionPool(PropertiesConfiguration configuration) throws SQLException {
        int maxActive = Integer.parseInt(configuration.getMaxActive());
        String url = configuration.getUrl();
        String user = configuration.getUser();
        String password = configuration.getPassword();
        for (int i = 0; i < maxActive; i++) {
            Connection currentConnection = DriverManager.getConnection(url, user, password);
            pool.add(currentConnection);
        }
    }

    public Connection getConnection() throws DaoException {
        try {
            semaphore.acquire();
            poolLock.lock();
            return pool.poll();
        } catch (InterruptedException e) {
            throw new DaoException(ERROR_WHILE_GETTING_A_CONNECTION, e);
        } finally {
            poolLock.unlock();
        }
    }

    public void returnConnection(Connection connection) {
        if (connection != null) {
            try {
                poolLock.lock();
                semaphore.release();
                pool.add(connection);
            } finally {
                poolLock.unlock();
            }
        }
    }

}
