package com.epam.pharmacy.beans;

import com.epam.pharmacy.dto.OrderDto;
import com.epam.pharmacy.dto.RecipeDto;
import com.epam.pharmacy.dto.InquiryDto;

import java.util.ArrayList;
import java.util.List;

public class SessionUser extends User {
    /**
     * Keeps a list of drugs that the user added to the order.
     */
    private Order order;
    /**
     * Keeps a list of drugs that found by last user search.
     */
    private List<Medicine> foundBySearch = new ArrayList<>();

    private List<RecipeDto> recipeDto = new ArrayList<>();
    private List<InquiryDto> inquiryDto = new ArrayList<>();
    private List<OrderDto> orderDto = new ArrayList<>();

    public SessionUser(User user) {
        super(user.getId(),
                user.getRole(),
                user.getName(),
                user.getSurname(),
                user.getLogin(),
                user.getPassword(),
                user.getTelephone()
        );
        order = new Order();
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public List<Medicine> getFoundBySearch() {
        return foundBySearch;
    }

    public void setFoundBySearch(List<Medicine> foundBySearch) {
        this.foundBySearch = foundBySearch;
    }

    public List<RecipeDto> getRecipeDto() {
        return recipeDto;
    }

    public void setRecipeDto(List<RecipeDto> recipeDto) {
        this.recipeDto = recipeDto;
    }

    public List<InquiryDto> getInquiryDto() {
        return inquiryDto;
    }

    public void setInquiryDto(List<InquiryDto> inquiryDto) {
        this.inquiryDto = inquiryDto;
    }

    public void setOrderDto(List<OrderDto> orderDto) {
        this.orderDto = orderDto;
    }

    public List<OrderDto> getOrderDto() {
        return orderDto;
    }

    public UserRole getUserRole(){
        return super.getRole();
    }

}
