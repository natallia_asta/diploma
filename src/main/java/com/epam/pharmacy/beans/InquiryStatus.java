package com.epam.pharmacy.beans;

public enum InquiryStatus {

    APPROVED("inquiry.status.approved"),
    REJECTED("inquiry.status.rejected"),
    UNDER_CONSIDERATION("inquiry.status.underConsideration");

    private String description;

    InquiryStatus(String description){
        this.description = description;
    }

    public String getDescription(){
        return description;
    }
}
