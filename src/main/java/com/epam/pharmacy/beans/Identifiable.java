package com.epam.pharmacy.beans;

public interface Identifiable<T> {
    int getId();
}
