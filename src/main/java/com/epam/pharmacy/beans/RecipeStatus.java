package com.epam.pharmacy.beans;

public enum RecipeStatus {

    OPEN("recipe.status.open"), CLOSED("recipe.status.closed"), EXPIRED("recipe.status.expired");

    private String description;

    private RecipeStatus(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }
}
