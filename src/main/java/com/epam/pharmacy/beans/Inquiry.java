package com.epam.pharmacy.beans;

import java.util.Objects;

public class Inquiry implements Identifiable {

    private int id;
    private int doctorId;
    private int recipeId;
    private int days;
    private InquiryStatus state;

    public Inquiry(int id, int doctorId, int recipeId, int days, InquiryStatus state) {
        this.id = id;
        this.doctorId = doctorId;
        this.recipeId = recipeId;
        this.days = days;
        this.state = state;
    }

    public Inquiry(int doctorId, int recipeId, int days) {
        this.doctorId = doctorId;
        this.recipeId = recipeId;
        this.days = days;
        state = InquiryStatus.UNDER_CONSIDERATION;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(int doctorId) {
        this.doctorId = doctorId;
    }

    public int getRecipeId() {
        return recipeId;
    }

    public void setRecipeId(int recipeId) {
        this.recipeId = recipeId;
    }

    public InquiryStatus getState() {
        return state;
    }

    public void setState(InquiryStatus state) {
        this.state = state;
    }

    public int getDays() {
        return days;
    }

    public void setDays(int days) {
        this.days = days;
    }

    @Override
    public int getId() {
        return 0;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Inquiry inquiry = (Inquiry) o;
        return id == inquiry.id &&
                doctorId == inquiry.doctorId &&
                recipeId == inquiry.recipeId &&
                days == inquiry.days &&
                state == inquiry.state;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, doctorId, recipeId, days, state);
    }
}
