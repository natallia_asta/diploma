package com.epam.pharmacy.beans;

import java.util.Objects;

public class Medicine implements Identifiable {
    private int id;
    private String tradeName;
    private String medicineInn;
    private float price;
    private int availableAmount;
    private boolean recipeRequired;
    private MedForm form;
    private String doze;
    private String producer;
    private int amount;

    public Medicine(){}

    public Medicine(int id, String tradeName, String medicineInn, float price, int availableAmount,
                    boolean recipeRequired, MedForm form, String doze, String producer) {
        this.id = id;
        this.tradeName = tradeName;
        this.medicineInn = medicineInn;
        this.price = price;
        this.availableAmount = availableAmount;
        this.recipeRequired = recipeRequired;
        this.form = form;
        this.doze = doze;
        this.producer = producer;
    }

    public int getId() {
        return id;
    }

    public String getTradeName() {
        return tradeName;
    }

    public String getMedicineInn() {
        return medicineInn;
    }

    public float getPrice() {
        return price;
    }

    public int getAvailableAmount() {
        return availableAmount;
    }

    public boolean isRecipeRequired() {
        return recipeRequired;
    }

    public MedForm getForm() {
        return form;
    }

    public String getDoze() {
        return doze;
    }

    public String getProducer() {
        return producer;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setTradeName(String tradeName) {
        this.tradeName = tradeName;
    }

    public void setMedicineInn(String medicineInn) {
        this.medicineInn = medicineInn;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public void setAvailableAmount(int availableAmount) {
        this.availableAmount = availableAmount;
    }

    public void setRecipeRequired(boolean recipeRequired) {
        this.recipeRequired = recipeRequired;
    }

    public void setForm(MedForm form) {
        this.form = form;
    }

    public void setDoze(String doze) {
        this.doze = doze;
    }

    public void setProducer(String producer) {
        this.producer = producer;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Medicine medicine = (Medicine) o;
        return id == medicine.id &&
                Float.compare(medicine.price, price) == 0 &&
                availableAmount == medicine.availableAmount &&
                recipeRequired == medicine.recipeRequired &&
                amount == medicine.amount &&
                Objects.equals(tradeName, medicine.tradeName) &&
                Objects.equals(medicineInn, medicine.medicineInn) &&
                form == medicine.form &&
                Objects.equals(doze, medicine.doze) &&
                Objects.equals(producer, medicine.producer);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, tradeName, medicineInn, price, availableAmount, recipeRequired, form, doze, producer, amount);
    }
}
