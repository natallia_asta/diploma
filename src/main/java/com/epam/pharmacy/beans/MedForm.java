package com.epam.pharmacy.beans;

public enum MedForm {

    TABLETS("medicine.form.tablets"),
    SYRUP("medicine.form.syrup"),
    CREAM("medicine.form.cream"),
    GEL("medicine.form.gel"),
    DROPS("medicine.form.drops"),
    SUPPOSITORIES("medicine.form.suppositories"),
    AMPOULES("medicine.form.ampoules");

    private String description;

    MedForm(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }
}
