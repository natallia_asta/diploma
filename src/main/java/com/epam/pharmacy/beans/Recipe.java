package com.epam.pharmacy.beans;

import java.time.LocalDate;
import java.util.Objects;

public class Recipe implements Identifiable {
    private int recipeId;
    private int number;
    private LocalDate validityDate;
    private int clientId;
    private int doctorId;
    private RecipeStatus status;

    public Recipe(){}

    public Recipe(int id, int clientId, int number, LocalDate validityDate, int doctorId, RecipeStatus status){
        this.recipeId = id;
        this.clientId = clientId;
        this.number = number;
        this.validityDate = validityDate;
        this.doctorId = doctorId;
        this.status = status;
    }

    public Recipe(int recipeId) {
        this.recipeId = recipeId;
    }

    public Recipe(int recipeId, int recipeNumber, int clientId, LocalDate validityDate) {
        this.recipeId = recipeId;
        this.clientId = clientId;
        number = recipeNumber;
        this.validityDate = validityDate;
    }

    public void setId(int id) {
        this.recipeId = id;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public LocalDate getValidityDate() {
        return validityDate;
    }

    public void setValidityDate(LocalDate validityDate) {
        this.validityDate = validityDate;
    }

    public int getClientId() {
        return clientId;
    }

    public void setClientId(int clientId) {
        this.clientId = clientId;
    }

    public int getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(int doctorId) {
        this.doctorId = doctorId;
    }


    public RecipeStatus getStatus() {
        return status;
    }

    public void setStatus(RecipeStatus status) {
        this.status = status;
    }

    @Override
    public int getId() {
        return recipeId;
    }

    public int getRecipeId(){
        return recipeId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Recipe recipe = (Recipe) o;
        return recipeId == recipe.recipeId &&
                number == recipe.number &&
                clientId == recipe.clientId &&
                doctorId == recipe.doctorId &&
                Objects.equals(validityDate, recipe.validityDate) &&
                status == recipe.status;
    }

    @Override
    public int hashCode() {
        return Objects.hash(recipeId, number, validityDate, clientId, doctorId, status);
    }
}
