package com.epam.pharmacy.beans;

import java.time.LocalDate;
import java.util.*;

public class Order implements Identifiable {
    int id;
    LocalDate date;
    int clientId;
    float totalPrice;
    int pharmacistId;
    boolean isOpen;
    /**
     * Keeps a list of drugs that the user added to the order.
     */
    List<Medicine> medicineList = new ArrayList<>();
    /**
     * Keeps a pairs of drugs and recipe.
     */
    Map<Medicine, Recipe> medicineRecipe = new HashMap<>();

    public Order() {}

    public Order(LocalDate date, int clientId, float totalPrice, int pharmacistId, List<Medicine> medicineList, boolean isOpen) {
        this.date = date;
        this.clientId = clientId;
        this.totalPrice = totalPrice;
        this.pharmacistId = pharmacistId;
        this.medicineList = medicineList;
        this.isOpen = isOpen;
    }

    public Order(int number, LocalDate date, float totalPrice, boolean isOpen) {
        id = number;
        this.date = date;
        this.totalPrice = totalPrice;
        this.isOpen = isOpen;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public int getClientId() {
        return clientId;
    }

    public void setClientId(int clientId) {
        this.clientId = clientId;
    }

    public float getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(float totalPrice) {
        this.totalPrice = totalPrice;
    }

    public int getPharmacistId() {
        return pharmacistId;
    }

    public void setPharmacistId(int pharmacistId) {
        this.pharmacistId = pharmacistId;
    }

    public boolean isOpen() {
        return isOpen;
    }

    public void setOpen(boolean open) {
        isOpen = open;
    }

    public List<Medicine> getMedicineList() {
        return medicineList;
    }

    public void setMedicineList(List<Medicine> medicineList) {
        this.medicineList = medicineList;
    }

    public Map<Medicine, Recipe> getMedicineRecipe() {
        return medicineRecipe;
    }

    public void setMedicineRecipe(Map<Medicine, String> medicineRecipeNumber) {
        this.medicineRecipe = medicineRecipe;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Order order = (Order) o;
        return id == order.id &&
                clientId == order.clientId &&
                Float.compare(order.totalPrice, totalPrice) == 0 &&
                pharmacistId == order.pharmacistId &&
                isOpen == order.isOpen &&
                Objects.equals(date, order.date) &&
                Objects.equals(medicineList, order.medicineList);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, date, clientId, totalPrice, pharmacistId, isOpen, medicineList);
    }
}
