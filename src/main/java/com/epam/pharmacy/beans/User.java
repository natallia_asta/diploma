package com.epam.pharmacy.beans;

import java.util.Objects;

public class User implements Identifiable {
    private int id;
    private UserRole role;
    private String name;
    private String surname;
    private String login;
    private String password;
    private String telephone;

    public User() {
        this("", "", "", "", "");
    }

    public User(String name, String surname, String login, String password, String telephone) {
        this(0, UserRole.CLIENT, name, surname, login, password, telephone);
    }

    public User(int id, UserRole role, String name, String surname, String login, String password, String telephone) {
        this.id = id;
        this.role = role;
        this.name = name;
        this.surname = surname;
        this.login = login;
        this.password = password;
        this.telephone = telephone;
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }

    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }

    public UserRole getRole() {
        return role;
    }

    public String getSurname() {
        return surname;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setRole(UserRole role) {
        this.role = role;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return id == user.id &&
                role == user.role &&
                Objects.equals(name, user.name) &&
                Objects.equals(surname, user.surname) &&
                Objects.equals(login, user.login) &&
                Objects.equals(password, user.password) &&
                Objects.equals(telephone, user.telephone);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, role, name, surname, login, password, telephone);
    }
}
