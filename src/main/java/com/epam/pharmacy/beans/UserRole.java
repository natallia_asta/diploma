package com.epam.pharmacy.beans;


public enum UserRole {

    CLIENT, PHARMACIST, DOCTOR
}
