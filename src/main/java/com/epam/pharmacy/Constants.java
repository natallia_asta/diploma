package com.epam.pharmacy;

public interface Constants {
    String KEY_USER = "user";
    String KEY_MESSAGE = "message";
    String KEY_ERROR_MESSAGE = "errorMsg";

    String VALUE_APPLICATION_ERROR = "error.appError";
    String VALUE_UNDER_DEVELOPMENT = "message.underDevelopment";
    String VALUE_NO_ITEMS_TO_DISPLAY = "message.noItemsToDisplay";

    String PARAMETER_COMMAND = "command";

    String ERROR_JSP = "error.jsp";
    String INDEX_JSP = "index.jsp";
    String REGISTRATION_JSP = "registration.jsp";
    String SEARCH_RESULT_JSP = "search_result.jsp";
    String SEARCH_JSP = "search.jsp";
    String USER_PAGE_JSP = "user_page.jsp";
    String PHARMACIST_PAGE_JSP = "pharmacist_page.jsp";
    String DOCTOR_PAGE_JSP = "doctor_page.jsp";
    String ORDER_JSP = "order.jsp";
    String ORDER_HISTORY_JSP = "order_history.jsp";
    String RECIPE_HISTORY_JSP = "recipe_history.jsp";
    String WRITE_DOCTOR_JSP = "write_doctor.jsp";
    String INQUIRY_HISTORY_JSP = "inquiry_history.jsp";
}
