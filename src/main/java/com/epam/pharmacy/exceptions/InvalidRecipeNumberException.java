package com.epam.pharmacy.exceptions;

public class InvalidRecipeNumberException extends Exception {

    public InvalidRecipeNumberException(String message) {
        super(message);
    }

    public InvalidRecipeNumberException(String message, Throwable cause) {
        super(message, cause);
    }
}
