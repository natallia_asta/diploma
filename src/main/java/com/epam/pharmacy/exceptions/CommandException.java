package com.epam.pharmacy.exceptions;

public class CommandException extends Exception {
    public CommandException(String s) {
        super(s);
    }

    public CommandException(String s, Throwable e) {
        super(s, e);
    }
}
