package com.epam.pharmacy.validator;

import com.epam.pharmacy.beans.User;
import com.epam.pharmacy.exceptions.InvalidUserDataException;
import com.epam.pharmacy.exceptions.ServiceException;
import com.epam.pharmacy.service.UserService;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Validates the correctness of user input during registration.
 */
public class UserRegistrationDataValidator {

    /** Pattern for name and surname. */
    private static final Pattern NAME_PATTERN = Pattern.compile("^[a-zA-Z]+$");

    /** Pattern for telephone number. */
    private static final Pattern PHONE_PATTERN = Pattern.compile("\\+[0-9]{3}\\([0-9]{2}\\)[0-9]{3}-[0-9]{2}-[0-9]{2}");

    /** Constant containing the key for message if passwords are not the same. */
    private static final String MESSAGE_NOT_SAME_PASSWORD = "error.notTheSamePassword";

    /** Constant containing the key for message if user input in the field "Name" is not match pattern. */
    private static final String MESSAGE_NOT_MATCH_NAME = "error.notMatchName";

    /** Constant containing the key for message if user input in the field "Surname" is not match pattern. */
    private static final String MESSAGE_NOT_MATCH_SURNAME = "error.notMatchSurname";

    /** Constant containing the key for message if user input in the field "Telephone" is not match pattern. */
    private static final String MESSAGE_NOT_MATCH_PHONE = "error.notMatchPhone";

    /** Constant containing the key for message if user input in the field "Login" is empty. */
    private static final String MESSAGE_NOT_MATCH_LOGIN = "error.notMatchLogin";

    /** Constant containing the key for message if user input in the field "Login" equals existing.  */
    private static final String MESSAGE_LOGIN_ALREADY_EXIST = "error.loginAlreadyExist";

    /** The variable is used for UserService instance storage. */
    private UserService userService;

    /**
     * Constructor with parameter.
     * @param userService is a UserService instance
     */
    public UserRegistrationDataValidator(UserService userService) {
        this.userService = userService;
    }

    /** Sets the value of userService variable. Needs only for testing. */
    /*package-private*/ void setUserService(UserService service) {
        userService = service;
    }

    /**
     *  Validates the correctness of user input during registration.
     *
     * @param user the instance of class User. The field values are set according to user input
     * @param passwordRepeat  the value input in the field "Password repeat"
     * @throws InvalidUserDataException if the user input is incorrect
     * @throws ServiceException if database error appears
     */
    public void validate(User user, String passwordRepeat) throws InvalidUserDataException, ServiceException {
        if (!user.getPassword().equals(passwordRepeat)) {
            throw new InvalidUserDataException(MESSAGE_NOT_SAME_PASSWORD);
        }
        nameValidate(user);
        surnameValidate(user);
        loginValidate(user);
        phoneValidate(user);
    }

    /**
     * Checks an input line is matching the pattern.
     *
     * @param line an user input
     * @param pattern an according pattern
     * @return true if the input matching the pattern, and false otherwise
     */
    private boolean isInputMatch(String line, Pattern pattern) {
        Matcher matcher = pattern.matcher(line);
        return matcher.find();
    }

    /**
     * Checks user input in the field "Name".
     *
     * @param user an instance of class User with the field values are set according to user input
     * @throws InvalidUserDataException containing clear and understandable for user
     * explanation why the input is incorrect
     */
    /*package-private*/ void nameValidate(User user) throws InvalidUserDataException {
        String name = user.getName();
        if (!isInputMatch(name, NAME_PATTERN)) {
            throw new InvalidUserDataException(MESSAGE_NOT_MATCH_NAME);
        }
    }

    /**
     * Checks user input in the field "Surname".
     *
     * @param user an instance of class User with the field values are set according to user input
     * @throws InvalidUserDataException containing clear and understandable for user
     * explanation why the input is incorrect
     */
    /*package-private*/ void surnameValidate(User user) throws InvalidUserDataException {
        String surname = user.getSurname();
        if (!isInputMatch(surname, NAME_PATTERN)) {
            throw new InvalidUserDataException(MESSAGE_NOT_MATCH_SURNAME);
        }
    }

    /**
     * Checks user input in the field "Telephone".
     *
     * @param user an instance of class User with the field values are set according to user input
     * @throws InvalidUserDataException containing clear and understandable for user
     * explanation why the input is incorrect
     */
    /*package-private*/ void phoneValidate(User user) throws InvalidUserDataException {
        String phone = user.getTelephone();
        if (!isInputMatch(phone, PHONE_PATTERN)) {
            throw new InvalidUserDataException(MESSAGE_NOT_MATCH_PHONE);
        }
    }

    /**
     * Checks user input in the field "Login".
     *
     * @param user an instance of class User with the field values are set according to user input
     * @throws InvalidUserDataException containing clear and understandable for user
     * explanation why the input is incorrect
     */
    /*package-private*/ void loginValidate(User user) throws InvalidUserDataException, ServiceException {
        String login = user.getLogin();
        if (login.length() == 0) {
            throw new InvalidUserDataException(MESSAGE_NOT_MATCH_LOGIN);
        }
        if (userService.isLoginAlreadyExist(login)) {
            throw new InvalidUserDataException(MESSAGE_LOGIN_ALREADY_EXIST);
        }
    }
}
