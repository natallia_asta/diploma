package com.epam.pharmacy.validator;

import com.epam.pharmacy.beans.Recipe;
import com.epam.pharmacy.beans.RecipeStatus;
import com.epam.pharmacy.exceptions.InvalidRecipeNumberException;
import com.epam.pharmacy.exceptions.ServiceException;
import com.epam.pharmacy.service.RecipeService;

import java.time.LocalDate;
import java.util.Optional;

public class RecipeNumberValidator {

    private static final String RECIPE_STATUS_CLOSED = "CLOSED";
    private static final String ERROR_RECIPE_NOT_REGISTERED = "error.recipeNotRegistered";
    private static final String ERROR_RECIPE_ALREADY_USED = "error.hasAlreadyBeenUsed";
    private static final String ERROR_EXPIRED_RECIPE = "error.expiredRecipe";
    private static final String ERROR_WRONG_RECIPE_DATA = "error.wrongRecipeData";

    private final RecipeService recipeService;

    public RecipeNumberValidator(RecipeService recipeService) {
        this.recipeService = recipeService;
    }

    public Recipe validate(String recipeNumber, int clientId, String tradeName, String amount)
            throws InvalidRecipeNumberException, ServiceException {

        Recipe recipe = findRecipe(recipeNumber, clientId);
        validateStatus(recipe);
        validateDate(recipe);
        validateMedicineAndAmount(recipeNumber, clientId, tradeName, amount);
        return recipe;
    }

    private Recipe findRecipe(String recipeNumber, int clientId) throws ServiceException, InvalidRecipeNumberException {
        Optional<Recipe> optionalRecipe;
        Recipe recipe;
        optionalRecipe = recipeService.search(recipeNumber, clientId);
        if (optionalRecipe.isPresent()) {
            recipe = optionalRecipe.get();
        } else {
            throw new InvalidRecipeNumberException(ERROR_RECIPE_NOT_REGISTERED);
        }
        return recipe;
    }

    private void validateStatus(Recipe recipe) throws InvalidRecipeNumberException {
        RecipeStatus status = recipe.getStatus();
        if (status.equals(RecipeStatus.valueOf(RECIPE_STATUS_CLOSED))) {
            throw new InvalidRecipeNumberException(ERROR_RECIPE_ALREADY_USED);
        }
    }

    private void validateDate(Recipe recipe) throws InvalidRecipeNumberException {
        LocalDate validityDate = recipe.getValidityDate();
        if (validityDate.isBefore(LocalDate.now())) {
            throw new InvalidRecipeNumberException(ERROR_EXPIRED_RECIPE);
        }
    }

    private void validateMedicineAndAmount(String recipeNumber, int userId, String tradeName, String amount)
            throws InvalidRecipeNumberException, ServiceException {

        Optional optionalRecipeDto = recipeService.getMedicineDataFromRecipeByParameters(recipeNumber, userId, tradeName, amount);
        if (!optionalRecipeDto.isPresent()) {
            throw new InvalidRecipeNumberException(ERROR_WRONG_RECIPE_DATA);
        }
    }

}



