package com.epam.pharmacy.service;

import com.epam.pharmacy.beans.Recipe;
import com.epam.pharmacy.dao.RecipeDao;
import com.epam.pharmacy.dao.RecipeDtoDao;
import com.epam.pharmacy.dto.RecipeDto;
import com.epam.pharmacy.exceptions.DaoException;
import com.epam.pharmacy.exceptions.ServiceException;

import java.util.List;
import java.util.Optional;

public class RecipeService extends AbstractService {

    private static final String DB_ERROR = "DB error, ";

    public Optional<Recipe> search(String recipeNumber, int clientId) throws ServiceException {
        Optional<Recipe> result;
        try {
            RecipeDao dao = daoFactory.getRecipeDao();
            result = dao.findRecipeByNumberAndClientId(recipeNumber, clientId);
        } catch (DaoException e) {
            throw new ServiceException(DB_ERROR + e.getMessage(), e);
        }
        return result;
    }

    public Optional<RecipeDto> getMedicineDataFromRecipeByParameters
            (String recipeNumber, int userId, String tradeName, String amount) throws ServiceException {

        Optional<RecipeDto> result;
        try {
            RecipeDtoDao dao = daoFactory.getRecipeDtoDao();
            result = dao.getRecipeDataByParameters(recipeNumber, userId, tradeName, amount);
        } catch (DaoException e) {
            throw new ServiceException(DB_ERROR + e.getMessage(), e);
        }
        return result;
    }

    public List<RecipeDto> getMedicineDataFromRecipeByUserId(int clientId) throws ServiceException {
        List<RecipeDto> result;
        try {
            RecipeDtoDao dao = daoFactory.getRecipeDtoDao();
            result = dao.getRecipeDataByClientId(clientId);
        } catch (DaoException e) {
            throw new ServiceException(DB_ERROR + e.getMessage(), e);
        }
        return result;
    }

    /**
     * package-private
     */
    void updateRecipeDataTable(int recipeId, int medicineId, int amount) throws ServiceException {
        RecipeDao dao = daoFactory.getRecipeDao();
        try {
            dao.updateRecipeMedicineTable(amount, recipeId, medicineId);
            dao.updateStatusIfAllRedeemed(recipeId);
        } catch (DaoException e) {
            throw new ServiceException(DB_ERROR + e.getMessage(), e);
        }
    }

    public void updateRecipeStatusIfExpired(int clientId) throws ServiceException {
        try {
            RecipeDao dao = daoFactory.getRecipeDao();
            dao.updateIfExpired(clientId);
        } catch (DaoException e) {
            throw new ServiceException(DB_ERROR + e.getMessage(), e);
        }
    }
}
