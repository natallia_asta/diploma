package com.epam.pharmacy.service;

import com.epam.pharmacy.beans.Medicine;
import com.epam.pharmacy.beans.Order;
import com.epam.pharmacy.dao.MedicineDao;
import com.epam.pharmacy.exceptions.DaoException;
import com.epam.pharmacy.exceptions.ServiceException;

import java.util.List;

public class MedicineService extends AbstractService {

    private static final String DB_ERROR = "DB error ";

    public List<Medicine> search(String name) throws ServiceException {
        List<Medicine> result;
        try {
            MedicineDao dao = daoFactory.getMedicineDao();
            result = dao.findMedicineByTradeName(name);
        } catch (DaoException e) {
            throw new ServiceException(DB_ERROR + e.getMessage(), e);
        }
        return result;
    }

    public List<Medicine> getAllMedicineList() throws ServiceException {
        List<Medicine> medicineList;
        try {
            MedicineDao dao = daoFactory.getMedicineDao();
            medicineList = dao.getAll();
        } catch (DaoException e) {
            throw new ServiceException(DB_ERROR + e.getMessage(), e);
        }
        return medicineList;
    }

    public void updateMedicineAvailableAmount(Order order) throws ServiceException {
        List<Medicine> medicineList = order.getMedicineList();
        try {
            MedicineDao dao = daoFactory.getMedicineDao();
            for (Medicine medicine : medicineList) {
                int medicineId = medicine.getId();
                int amount = medicine.getAmount();
                dao.updateAmount(medicineId, amount);
            }
        } catch (DaoException e) {
            throw new ServiceException(DB_ERROR + e.getMessage(), e);
        }
    }
}
