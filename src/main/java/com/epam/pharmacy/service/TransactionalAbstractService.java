package com.epam.pharmacy.service;

import com.epam.pharmacy.exceptions.DaoException;
import com.epam.pharmacy.exceptions.ServiceException;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * Class with methods for supporting of the transactions.
 */
public abstract class TransactionalAbstractService extends AbstractService {

    private static final String THE_PREVIOUS_TRANSACTION_IS_NOT_FINISHED_YET =
            "The previous transaction is not finished yet.";
    private static final String IS_NO_ANY_TRANSACTION_STARTED_YET =
            "Here is no any transaction started yet.";
    private static final String CAN_NOT_CHANGE_AUTO_COMMIT_STATE =
            "Can not change autoCommit state.";
    private static final String CAN_NOT_RESTORE_AUTO_COMMIT_STATE =
            "Can not restore autoCommit state.";
    /**
     * Container holds a Connection auto-commit mode. Has three states: "true", "false", null:
     * "true" and "false" are the modes of the auto-commit
     * null is a sign that the transaction does not started (finished), or does not exist.
     */
    private Boolean autoCommitState;

    /**
     * Checks the value of autoCommitState is not null otherwise trows exception.
     * If it's not determined (is null) then saves a Connection auto-commit state.
     * Sets the Connection auto-commit mode to the state "false".
     *
     * @throws DaoException when the value of this autoCommitState already has been determined
     *                      or in case of database error.
     */
    protected void saveStateAnsStartTransaction() throws ServiceException {
        try {
            if (autoCommitState != null) {
                throw new ServiceException(THE_PREVIOUS_TRANSACTION_IS_NOT_FINISHED_YET);
            }
            final Connection connection = daoFactory.getConnection();
            autoCommitState = connection.getAutoCommit();
            connection.setAutoCommit(false);
        } catch (SQLException e) {
            throw new ServiceException(CAN_NOT_CHANGE_AUTO_COMMIT_STATE, e);
        }
    }

    /**
     * Checks the value of the autoCommitState. If it's not determined (is null) then throws exception.
     * Sets the Connection auto-commit mode to the value equals this autoCommitState value.
     * Then sets the value of this autoCommitState equals null.
     *
     * @throws DaoException if the value of this autoCommitState is not determined,
     *                      or in the case of database error
     */
    protected void closeTransactionAndRestoreState() throws ServiceException {
        try {
            if (autoCommitState == null) {
                throw new ServiceException(IS_NO_ANY_TRANSACTION_STARTED_YET);
            }
            final Connection connection = daoFactory.getConnection();
            connection.setAutoCommit(autoCommitState);
            autoCommitState = null;
        } catch (SQLException e) {
            throw new ServiceException(CAN_NOT_RESTORE_AUTO_COMMIT_STATE, e);
        }
    }

    // execute your business logic here
    protected abstract void doTransaction() throws ServiceException;

    protected void transaction() throws ServiceException {
        final Connection connection = daoFactory.getConnection();
        saveStateAnsStartTransaction();

        try {
            doTransaction();
        } catch (ServiceException ex) {
            try {
                connection.rollback();
            } catch (SQLException sqle) {
                ex.addSuppressed(sqle);
            }
            throw ex;
        }

        try {
            connection.commit();
        } catch (SQLException e) {
            throw new ServiceException(e);
        }

        closeTransactionAndRestoreState();
    }
}
