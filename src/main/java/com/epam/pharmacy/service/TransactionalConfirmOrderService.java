package com.epam.pharmacy.service;

import com.epam.pharmacy.beans.Medicine;
import com.epam.pharmacy.beans.Order;
import com.epam.pharmacy.beans.Recipe;
import com.epam.pharmacy.dao.impl.DaoFactory;
import com.epam.pharmacy.exceptions.ServiceException;

import java.time.LocalDate;
import java.util.Map;

public class TransactionalConfirmOrderService extends TransactionalAbstractService {

    private static final String TRANSACTION_EXCEPTION = "Transaction exception ";

    private final RecipeService recipeService;
    private final OrderService orderService;
    private final MedicineService medicineService;

    // variables for transaction
    private int orderNumber;
    private Order order;
    private int userId;

    public TransactionalConfirmOrderService() {
        recipeService = new RecipeService();
        orderService = new OrderService();
        medicineService = new MedicineService();
    }

    @Override
    public void setDaoFactory(DaoFactory daoFactory) {
        super.setDaoFactory(daoFactory);
        recipeService.setDaoFactory(daoFactory);
        orderService.setDaoFactory(daoFactory);
        medicineService.setDaoFactory(daoFactory);
    }

    @Override
    protected void doTransaction() throws ServiceException {
        updateRecipeData(order);
        orderNumber = createOrderAndGetId(order, userId);
        medicineService.updateMedicineAvailableAmount(order);
    }

    public int createOrderAndUpdateRecipeAndMedicineData(Order order, int userId) throws ServiceException {
        this.order = order;
        this.userId = userId;

        transaction(); // calls doTransaction

        this.order.getMedicineList().clear();
        this.order.getMedicineRecipe().clear();
        this.order.setTotalPrice(0);
        return orderNumber;
    }

    private void updateRecipeData(Order order) throws ServiceException {
        Map<Medicine, Recipe> medicineRecipe = order.getMedicineRecipe();
        for (Map.Entry<Medicine, Recipe> entry : medicineRecipe.entrySet()) {
            final Medicine medicine = entry.getKey();
            final Recipe recipe = entry.getValue();
            int medicineId = medicine.getId();
            int recipeId = recipe.getId();
            int amount = medicine.getAmount();
            recipeService.updateRecipeDataTable(recipeId, medicineId, amount);
        }
    }

    private int createOrderAndGetId(Order order, int userId) throws ServiceException {
        orderService.calculateAndSetOrderTotalPrice(order);
        order.setClientId(userId);
        order.setDate(LocalDate.now());
        return orderService.putOrder(order);
    }
}
