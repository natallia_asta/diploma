package com.epam.pharmacy.service;

import com.epam.pharmacy.dao.impl.DaoFactory;

public abstract class AbstractService {

    protected DaoFactory daoFactory;

    public void setDaoFactory(DaoFactory daoFactory) {
        this.daoFactory = daoFactory;
    }
}
