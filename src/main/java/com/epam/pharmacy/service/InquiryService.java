package com.epam.pharmacy.service;

import com.epam.pharmacy.beans.Inquiry;
import com.epam.pharmacy.dao.InquiryDao;
import com.epam.pharmacy.dao.InquiryDtoDao;
import com.epam.pharmacy.dto.InquiryDto;
import com.epam.pharmacy.exceptions.DaoException;
import com.epam.pharmacy.exceptions.ServiceException;

import java.util.List;

public class InquiryService extends AbstractService {

    private static final String DB_ERROR = "DB error, ";

    public int createInquiry(int doctorId, int recipeId, int days) throws ServiceException {
        Inquiry inquiry = new Inquiry(doctorId, recipeId, days);
        try {
            InquiryDao dao = daoFactory.getInquiryDao();
            return dao.create(inquiry);
        } catch (DaoException e) {
            throw new ServiceException(DB_ERROR + e.getMessage(), e);
        }
    }

    public List<InquiryDto> getDataFromInquiryAndRecipeByDoctorId(int doctorId) throws ServiceException {
        List<InquiryDto> result;
        try {
            InquiryDtoDao dao = daoFactory.getInquiryDtoDao();
            result = dao.getInquiryRecipeDataByDoctorId(doctorId);
        } catch (DaoException e) {
            throw new ServiceException(DB_ERROR + e.getMessage(), e);
        }
        return result;
    }

    public List<InquiryDto> getDataFromInquiryAndRecipeByClientId(int clientId) throws ServiceException {
        List<InquiryDto> result;
        try {
            InquiryDtoDao dao = daoFactory.getInquiryDtoDao();
            result = dao.getInquiryRecipeDataByClientId(clientId);
        } catch (DaoException e) {
            throw new ServiceException(DB_ERROR + e.getMessage(), e);
        }
        return result;
    }
}
