package com.epam.pharmacy.service;

import com.epam.pharmacy.beans.User;
import com.epam.pharmacy.dao.UserDao;
import com.epam.pharmacy.exceptions.DaoException;
import com.epam.pharmacy.exceptions.ServiceException;

import java.util.Optional;

public class UserService extends AbstractService {

    private static final String DB_ERROR = "DB error, ";

    public Optional<User> login(String login, String password) throws ServiceException {
        try {
            UserDao dao = daoFactory.getUserDao();
            return dao.findUserByLoginAndPassword(login, password);
        } catch (DaoException e) {
            throw new ServiceException(DB_ERROR + e.getMessage(), e);
        }
    }

    public int register(User user) throws ServiceException {
        try {
            UserDao dao = daoFactory.getUserDao();
            return dao.create(user);
        } catch (DaoException e) {
            throw new ServiceException(DB_ERROR + e.getMessage(), e);
        }
    }

    public boolean isLoginAlreadyExist(String login) throws ServiceException {
        try {
            UserDao dao = daoFactory.getUserDao();
            return dao.isLoginExist(login);
        } catch (DaoException e) {
            throw new ServiceException(DB_ERROR + e.getMessage(), e);
        }
    }

}
