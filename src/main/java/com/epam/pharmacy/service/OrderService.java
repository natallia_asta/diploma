package com.epam.pharmacy.service;

import com.epam.pharmacy.beans.Medicine;
import com.epam.pharmacy.beans.Order;
import com.epam.pharmacy.beans.Recipe;
import com.epam.pharmacy.dao.OrderDao;
import com.epam.pharmacy.dao.OrderDtoDao;
import com.epam.pharmacy.dto.OrderDto;
import com.epam.pharmacy.exceptions.DaoException;
import com.epam.pharmacy.exceptions.ServiceException;

import java.util.List;
import java.util.Map;

public class OrderService extends AbstractService {

    private static final String DB_ERROR = "DB error, ";

    /*package-private*/ int putOrder(Order order) throws ServiceException {
        try {
            OrderDao dao = daoFactory.getOrderDao();
            return dao.create(order);
        } catch (DaoException e) {
            throw new ServiceException(DB_ERROR + e.getMessage(), e);
        }
    }

    public float calculateAndSetOrderTotalPrice(Order order) {
        final List<Medicine> dragList = order.getMedicineList();
        float totalPrice = 0;
        for (Medicine medicine : dragList) {
            final int amount = medicine.getAmount();
            totalPrice += medicine.getPrice() * amount;
        }
        order.setTotalPrice(totalPrice);
        return totalPrice;
    }

    public void removeSelectedItems(Order order, String[] selectedCheckbox) {
        final List<Medicine> dragList = order.getMedicineList();
        final Map<Medicine, Recipe> medicineRecipe = order.getMedicineRecipe();
        for (int i = selectedCheckbox.length - 1; i >= 0; i--) {
            final String line = selectedCheckbox[i];
            final int checkboxNum = Integer.parseInt(line);
            Medicine medicine = dragList.get(checkboxNum);
            dragList.remove(medicine);
            medicineRecipe.remove(medicine);
        }
    }

    public List<OrderDto> getOrderDataByClientId(int id) throws ServiceException {
        try {
            final OrderDtoDao dao = daoFactory.getOrderDtoDao();
            return dao.getOrderDataById(id);
        } catch (DaoException e) {
            throw new ServiceException(DB_ERROR + e.getMessage(), e);
        }
    }
}

