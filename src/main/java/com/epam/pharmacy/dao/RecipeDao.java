package com.epam.pharmacy.dao;

import com.epam.pharmacy.beans.Recipe;
import com.epam.pharmacy.exceptions.DaoException;

import java.util.Optional;

public interface RecipeDao extends Dao<Recipe>{

    Optional<Recipe> findRecipeByNumberAndClientId(String recipeNumber, int clientId) throws DaoException;

    void updateRecipeMedicineTable(int amount, int recipeId, int medicineId) throws DaoException;

    void updateStatusIfAllRedeemed(int recipeId) throws DaoException ;

    void updateIfExpired(int clientId) throws DaoException;
}
