package com.epam.pharmacy.dao.impl;

import com.epam.pharmacy.beans.User;
import com.epam.pharmacy.builder.Builder;
import com.epam.pharmacy.builder.UserBuilder;
import com.epam.pharmacy.dao.AbstractDao;
import com.epam.pharmacy.dao.UserDao;
import com.epam.pharmacy.exceptions.DaoException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.util.Optional;


public class UserDaoImpl extends AbstractDao<User> implements UserDao {

    private static final String SELECT_USER_BY_LOGIN = "SELECT * FROM `pharmacy`.`user` WHERE `login`=?";
    private static final String SELECT_USER_BY_LOGIN_AND_PASSWORD = "SELECT * FROM `pharmacy`.`user` WHERE `login`=? AND `password`=?";
    private static final String INSERT_USER =
            "INSERT INTO `user` (`login`, `password`, `role`, `name`, `surrname`, `phone`) " +
                    "VALUES (?, ?, ?, ?, ?, ?)";

    /*package-private*/ UserDaoImpl(Connection connection) {
        super(connection);
    }

    @Override
    public boolean isLoginExist(String login) throws DaoException {
        Builder builder = new UserBuilder();
        Optional user = executeForSingleResult(connection, SELECT_USER_BY_LOGIN, builder, login);
        return user.isPresent();
    }

    @Override
    public int createRequestToDoctor(int clientId, int recipeId, String days) throws DaoException {
        return 0;
    }

    @Override
    public Optional<User> findUserByLoginAndPassword(String login, String password) throws DaoException {
        Builder builder = new UserBuilder();
        return executeForSingleResult(connection, SELECT_USER_BY_LOGIN_AND_PASSWORD, builder, login, password);
    }


    @Override
    public Integer create(User entity) throws DaoException {
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            statement = connection.prepareStatement(INSERT_USER, Statement.RETURN_GENERATED_KEYS);
            statement.setString(1, entity.getLogin());
            statement.setString(2, entity.getPassword());
            statement.setString(3, String.valueOf(entity.getRole()));
            statement.setString(4, entity.getName());
            statement.setString(5, entity.getSurname());
            statement.setString(6, entity.getTelephone());
            statement.executeUpdate();
            resultSet = statement.getGeneratedKeys();
            if (resultSet.next()) {
                return resultSet.getInt(1);
            } else {
                throw new DaoException("User creation error");
            }
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            closeResultSet(resultSet);
            closePreparedStatement(statement);
        }
    }

    @Override
    public User read(Integer identity) {
        return null;
    }

    @Override
    public void update(User entity) {

    }

    @Override
    public void delete(User entity) {

    }

}
