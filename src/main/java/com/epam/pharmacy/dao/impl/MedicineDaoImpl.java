package com.epam.pharmacy.dao.impl;

import com.epam.pharmacy.beans.Medicine;
import com.epam.pharmacy.builder.Builder;
import com.epam.pharmacy.builder.MedicineBuilder;
import com.epam.pharmacy.dao.AbstractDao;
import com.epam.pharmacy.dao.MedicineDao;
import com.epam.pharmacy.exceptions.DaoException;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

public class MedicineDaoImpl extends AbstractDao<Medicine> implements MedicineDao {

    private static final String SELECT_BY_NAME =
            "SELECT * FROM `medicine` WHERE `trade_name`=?";
    private static final String SELECT_BY_INN =
            "SELECT * FROM `medicine` WHERE `medicine_inn` REGEXP '?'";
    private static final String GET_ALL =
            "SELECT * FROM `medicine`";
    private static final String UPDATE_MEDICINE_AVAILABLE_AMOUNT =
            "UPDATE `medicine` SET `available_amount` = `available_amount` - ?  WHERE `medicine_id` = ?";

    private static final String OPERATION_IS_NOT_SUPPORTED = "Operation is not supported";

    /*package-private*/ MedicineDaoImpl(Connection connection) {
        super(connection);
    }

    @Override
    public List<Medicine> findMedicineByTradeName(String name) throws DaoException {
        Builder builder = new MedicineBuilder();
        return executeQuery(connection, SELECT_BY_NAME, builder, name);
    }

    @Override
    public List<Medicine> findMedicineByContainingInn(String inn) throws DaoException {
        Builder builder = new MedicineBuilder();
        return executeQuery(connection, SELECT_BY_INN, builder, inn);
    }

    @Override
    public List<Medicine> getAll() throws DaoException {
        Builder builder = new MedicineBuilder();
        return (List<Medicine>) executeQuery(connection, GET_ALL, builder);
    }

    @Override
    public void updateAmount(int medicineId, int amount) throws DaoException {
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(UPDATE_MEDICINE_AVAILABLE_AMOUNT);
            statement.setInt(1, amount);
            statement.setInt(2, medicineId);
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            closePreparedStatement(statement);
        }
    }

    @Override
    public Integer create(Medicine entity) {
        throw new UnsupportedOperationException(OPERATION_IS_NOT_SUPPORTED);
    }

    @Override
    public Medicine read(Integer identity) {
        throw new UnsupportedOperationException(OPERATION_IS_NOT_SUPPORTED);
    }

    @Override
    public void update(Medicine entity) {
        throw new UnsupportedOperationException(OPERATION_IS_NOT_SUPPORTED);
    }

    @Override
    public void delete(Medicine entity) {
        throw new UnsupportedOperationException(OPERATION_IS_NOT_SUPPORTED);
    }

}
