package com.epam.pharmacy.dao.impl;

import com.epam.pharmacy.builder.Builder;
import com.epam.pharmacy.builder.InquiryDtoBuilder;
import com.epam.pharmacy.dao.AbstractDao;
import com.epam.pharmacy.dao.InquiryDtoDao;
import com.epam.pharmacy.dto.InquiryDto;
import com.epam.pharmacy.exceptions.DaoException;

import java.sql.Connection;
import java.util.List;

public class InquiryDtoDaoImpl extends AbstractDao<InquiryDto> implements InquiryDtoDao {

    private static final String GET_INQUIRY_DATA_BY_DOCTOR_ID =
            "SELECT `recipe_id`, `inquiry_id`, `number`, `client_id`, `trade_name`, `amount`, `not_redeemed`, `days`, " +
                    "`validity_date`, `inquiry`.`status`" +
                    "FROM `inquiry`" +
                    "JOIN `recipe_medicine_list` USING(`recipe_id`)" +
                    "JOIN `recipe` USING(`recipe_id`)" +
                    "JOIN `medicine` USING(`medicine_id`)" +
                    "WHERE `inquiry`.`doctor_id` = ? ORDER BY `inquiry_id` DESC";

    private static final String GET_INQUIRY_DATA_BY_CLIENT_ID =
            "SELECT `recipe_id`, `inquiry_id`, `number`, `client_id`, `trade_name`, `amount`, `not_redeemed`, `days`," +
                    " `validity_date`, `inquiry`.`status`" +
                    "FROM `inquiry`" +
                    "JOIN `recipe_medicine_list` USING(`recipe_id`)" +
                    "JOIN `recipe` USING(`recipe_id`)" +
                    "JOIN `medicine` USING(`medicine_id`)" +
                    "WHERE `client_id` = ? ORDER BY `inquiry_id` DESC";

    /*package-private*/ InquiryDtoDaoImpl(Connection connection) {
        super(connection);
    }

    @Override
    public List<InquiryDto> getInquiryRecipeDataByDoctorId(int userId) throws DaoException {
        String  doctorId = String.valueOf(userId);
        Builder builder = new InquiryDtoBuilder();
        return executeQuery(connection, GET_INQUIRY_DATA_BY_DOCTOR_ID, builder, doctorId);
    }

    @Override
    public List<InquiryDto> getInquiryRecipeDataByClientId(int userId) throws DaoException {
        String  clientId = String.valueOf(userId);
        Builder builder = new InquiryDtoBuilder();
        return executeQuery(connection, GET_INQUIRY_DATA_BY_CLIENT_ID, builder, clientId);
    }

    @Override
    public Integer create(InquiryDto entity)  {
        return null;
    }

    @Override
    public InquiryDto read(Integer identity) {
        return null;
    }

    @Override
    public void update(InquiryDto entity) {

    }

    @Override
    public void delete(InquiryDto entity) {

    }

}
