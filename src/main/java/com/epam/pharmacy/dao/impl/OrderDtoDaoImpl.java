package com.epam.pharmacy.dao.impl;

import com.epam.pharmacy.builder.Builder;
import com.epam.pharmacy.builder.OrderDtoBuilder;
import com.epam.pharmacy.dao.AbstractDao;
import com.epam.pharmacy.dao.OrderDtoDao;
import com.epam.pharmacy.dto.OrderDto;
import com.epam.pharmacy.exceptions.DaoException;

import java.sql.Connection;
import java.util.List;

public class OrderDtoDaoImpl extends AbstractDao<OrderDto> implements OrderDtoDao {

    private static final String GET_ORDER_DATA_BY_CLIENT_ID =
            "SELECT `order_id`, `trade_name`, `amount`, `is_open`, `date`, `total_price` " +
                    "FROM `order` " +
                    "JOIN `order_medicine_list` using (`order_id`) " +
                    "JOIN `medicine` using(`medicine_id`) " +
                    "WHERE `client_id` = ? " +
                    "ORDER BY `order_id` DESC";

    /*package-private*/ OrderDtoDaoImpl(Connection connection) {
        super(connection);
    }

    @Override
    public List<OrderDto> getOrderDataById(int id) throws DaoException {
        String clientId = String.valueOf(id);
        Builder builder = new OrderDtoBuilder();
        return executeQuery(connection, GET_ORDER_DATA_BY_CLIENT_ID, builder, clientId);
    }

    @Override
    public Integer create(OrderDto entity) throws DaoException {
        return null;
    }

    @Override
    public OrderDto read(Integer identity) {
        return null;
    }

    @Override
    public void update(OrderDto entity) throws DaoException {

    }

    @Override
    public void delete(OrderDto entity) {

    }
}
