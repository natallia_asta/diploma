package com.epam.pharmacy.dao.impl;

import com.epam.pharmacy.dao.InquiryDao;
import com.epam.pharmacy.dao.InquiryDtoDao;
import com.epam.pharmacy.dao.MedicineDao;
import com.epam.pharmacy.dao.OrderDao;
import com.epam.pharmacy.dao.OrderDtoDao;
import com.epam.pharmacy.dao.RecipeDao;
import com.epam.pharmacy.dao.RecipeDtoDao;
import com.epam.pharmacy.dao.UserDao;

import java.sql.Connection;

/**
 * Stores an instance of the Connection class and, at any appeals to the database, transfers this connection
 * to a specific dao. A creation of the dao instance does not occur through a call to the constructor,
 * but only through an invocation to the corresponding DaoFactory method. This is necessary for the correct execution
 * of the transaction, when it may be necessary to appeal to different dao, and they must use the same connection.
 */
public class DaoFactory {

    /** An instance of a Connection class. */
    private Connection connection;

    /** Constructor with parameter, sets the value of the connection. */
    public DaoFactory(Connection connection) {
        this.connection = connection;
    }

    /** Returns the connection. */
    public Connection getConnection() {
        return connection;
    }

    /** Returns the instance of the UserDaoImpl with set value of the connection. */
    public UserDao getUserDao() {
        return new UserDaoImpl(connection);
    }

    /** Returns the instance of the MedicineDaoImpl with set value of the connection. */
    public MedicineDao getMedicineDao() {
        return new MedicineDaoImpl(connection);
    }

    /** Returns the instance of the OrderDaoImpl with set value of the connection. */
    public OrderDao getOrderDao() {
        return new OrderDaoImpl(connection);
    }

    /** Returns the instance of the RecipeDaoImpl with set value of the connection. */
    public RecipeDao getRecipeDao() {
        return new RecipeDaoImpl(connection);
    }

    /** Returns the instance of the RecipeDtoDaoImpl with set value of the connection. */
    public RecipeDtoDao getRecipeDtoDao() {
        return new RecipeDtoDaoImpl(connection);
    }

    /** Returns the instance of the InquiryDaoImpl with set value of the connection. */
    public InquiryDao getInquiryDao() {
        return new InquiryDaoImpl(connection);
    }

    /** Returns the instance of the InquiryDtoDaoImpl with set value of the connection. */
    public InquiryDtoDao getInquiryDtoDao() {
        return new InquiryDtoDaoImpl(connection);
    }

    /** Returns the instance of the OrderDtoDaoImpl with set value of the connection. */
    public OrderDtoDao getOrderDtoDao() {
        return new OrderDtoDaoImpl(connection);
    }
}
