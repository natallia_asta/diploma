package com.epam.pharmacy.dao.impl;

import com.epam.pharmacy.beans.Medicine;
import com.epam.pharmacy.beans.Order;
import com.epam.pharmacy.dao.AbstractDao;
import com.epam.pharmacy.dao.OrderDao;
import com.epam.pharmacy.exceptions.DaoException;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

public class OrderDaoImpl extends AbstractDao<Order> implements OrderDao {

    private static final String INSERT_INTO_TABLE_ORDER =
            "INSERT INTO `order` (`date`, `client_id`, `total_price`) VALUES (?, ?, ?)";
    private static final String INSERT_INTO_TABLE_ORDER_MEDICINE_LIST =
            "INSERT INTO `order_medicine_list` VALUES (?, ?, ?)";

    private static final String ORDER_CREATION_ERROR = "Order creation error. ";


    /*package-private*/ OrderDaoImpl (Connection connection) {
        super(connection);
    }

    @Override
    public Integer create(Order entity) throws DaoException {
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            statement = connection.prepareStatement(INSERT_INTO_TABLE_ORDER, Statement.RETURN_GENERATED_KEYS);
            statement.setDate(1, Date.valueOf(entity.getDate()));
            statement.setInt(2, entity.getClientId());
            statement.setFloat(3, entity.getTotalPrice());
            statement.executeUpdate();
            resultSet = statement.getGeneratedKeys();
            if (resultSet.next()) {
                int order_id = resultSet.getInt(1);
                updateOrderMedicineListTable(entity, order_id);
                return resultSet.getInt(1);
            } else {
                throw new DaoException(ORDER_CREATION_ERROR);
            }
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            closeResultSet(resultSet);
            closePreparedStatement(statement);
        }

    }

    @Override
    public void updateOrderMedicineListTable(Order order, int order_id) throws DaoException {
        try {
            PreparedStatement statement = connection.prepareStatement(INSERT_INTO_TABLE_ORDER_MEDICINE_LIST);
            List<Medicine> medicineList = order.getMedicineList();
            for (Medicine medicine : medicineList) {
                int medicineId = medicine.getId();
                int amount = medicine.getAmount();
                statement.setInt(1, order_id);
                statement.setInt(2, medicineId);
                statement.setInt(3, amount);
                statement.executeUpdate();
            }
        } catch (SQLException e) {
            throw new DaoException(e);
        }
    }

    @Override
    public Order read(Integer identity) {
        return null;
    }

    @Override
    public void update(Order entity) {

    }

    @Override
    public void delete(Order entity) {

    }

}
