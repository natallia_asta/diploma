package com.epam.pharmacy.dao.impl;

import com.epam.pharmacy.builder.Builder;
import com.epam.pharmacy.builder.RecipeDtoBuilder;
import com.epam.pharmacy.dao.AbstractDao;
import com.epam.pharmacy.dao.RecipeDtoDao;
import com.epam.pharmacy.dto.RecipeDto;
import com.epam.pharmacy.exceptions.DaoException;

import java.sql.Connection;
import java.util.List;
import java.util.Optional;

public class RecipeDtoDaoImpl extends AbstractDao<RecipeDto> implements RecipeDtoDao {

    private static final String SELECT_RECIPE_DATA_BY_PARAMETERS =
            "SELECT `recipe_id`, `client_id`, `number`, `validity_date`, `trade_name`, `amount`, `doctor_id`, `status`, `medicine_inn`, `not_redeemed` " +
                    "FROM `recipe_medicine_list`" +
                    "JOIN `recipe` USING(`recipe_id`) " +
                    "JOIN `medicine` USING(`medicine_id`) " +
                    "WHERE `number` = ? AND `client_id` = ? AND `trade_name`= ? AND `not_redeemed` >= ? ";


    private static final String GET_RECIPE_DATA_BY_CLIENT_ID =
            "SELECT `recipe_id`, `client_id`, `number`, `validity_date`, `trade_name`, `amount`, `doctor_id`, `status`, `medicine_inn`, `not_redeemed` " +
                    "FROM `recipe_medicine_list`" +
                    "JOIN `recipe` USING(`recipe_id`) " +
                    "JOIN `medicine` USING(`medicine_id`) " +
                    "WHERE `client_id` = ?";

    /*package-private*/ RecipeDtoDaoImpl(Connection connection) {
        super(connection);
    }

    @Override
    public Optional<RecipeDto> getRecipeDataByParameters
            (String recipeNumber, int userId, String tradeName, String amount)
            throws DaoException {
        Builder builder = new RecipeDtoBuilder();
        String clientId = String.valueOf(userId);
        return executeForSingleResult(connection, SELECT_RECIPE_DATA_BY_PARAMETERS, builder, recipeNumber, clientId, tradeName, amount);
    }

    @Override
    public List<RecipeDto> getRecipeDataByClientId(int clientId) throws DaoException {
        String stringClientId = String.valueOf(clientId);
        Builder builder = new RecipeDtoBuilder();
        return executeQuery(connection, GET_RECIPE_DATA_BY_CLIENT_ID, builder, stringClientId);
    }

    @Override
    public Integer create(RecipeDto entity) {
        return null;
    }

    @Override
    public RecipeDto read(Integer identity) {
        return null;
    }

    @Override
    public void update(RecipeDto entity) {

    }

    @Override
    public void delete(RecipeDto entity) {

    }
}
