package com.epam.pharmacy.dao.impl;

import com.epam.pharmacy.beans.Recipe;
import com.epam.pharmacy.builder.Builder;
import com.epam.pharmacy.builder.RecipeBuilder;
import com.epam.pharmacy.dao.AbstractDao;
import com.epam.pharmacy.dao.RecipeDao;
import com.epam.pharmacy.exceptions.DaoException;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Optional;

public class RecipeDaoImpl extends AbstractDao<Recipe> implements RecipeDao {

    private static final String SELECT_RECIPE_BY_NUMBER_AND_CLIENT_ID =
            "SELECT * FROM `recipe` " + "WHERE `number`=? AND `client_id`=?";

    private static final String UPDATE_TABLE_RECIPE_MEDICINE_LIST =
            "UPDATE `recipe_medicine_list` " +
                    "SET `not_redeemed` = `not_redeemed`- ? " +
                    "WHERE `recipe_id` = ? AND `medicine_id` = ?";

    private static final String UPDATE_STATUS_IF_REDEEMED =
            "UPDATE `recipe` SET status='CLOSED' " +
                    "WHERE `recipe_id`=? " +
                    "AND 0=(" +
                    "SELECT SUM(`not_redeemed`)" +
                    "FROM `recipe_medicine_list`" +
                    "WHERE  `recipe_id`=?)";

    private static final String UPDATE_STATUS_IF_EXPIRED =
            "UPDATE `recipe` SET status='EXPIRED' WHERE `validity_date`< date(now()) AND client_id=?";

    /*package-private*/ RecipeDaoImpl(Connection connection) {
        super(connection);
    }

    @Override
    public Optional<Recipe> findRecipeByNumberAndClientId(String recipeNumber, int clientId)
            throws DaoException {
        String stringClientId = String.valueOf(clientId);
        Builder builder = new RecipeBuilder();
        return executeForSingleResult(connection, SELECT_RECIPE_BY_NUMBER_AND_CLIENT_ID, builder, recipeNumber, stringClientId);
    }

    @Override
    public void updateStatusIfAllRedeemed(int recipeId) throws DaoException {
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(UPDATE_STATUS_IF_REDEEMED);
            statement.setInt(1, recipeId);
            statement.setInt(2, recipeId);
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            closePreparedStatement(statement);
        }
    }

    @Override
    public void updateIfExpired(int clientId) throws DaoException {
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(UPDATE_STATUS_IF_EXPIRED);
            statement.setInt(1, clientId);
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            closePreparedStatement(statement);
        }
    }

    @Override
    public void updateRecipeMedicineTable(int amount, int recipeId, int medicineId) throws DaoException {
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(UPDATE_TABLE_RECIPE_MEDICINE_LIST);
            statement.setInt(1, amount);
            statement.setInt(2, recipeId);
            statement.setInt(3, medicineId);
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            closePreparedStatement(statement);
        }
    }

    @Override
    public void delete(Recipe entity) {
    }

    @Override
    public void update(Recipe entity) {
    }

    @Override
    public Integer create(Recipe entity) {
        return null;
    }

    @Override
    public Recipe read(Integer identity) {
        return null;
    }

}
