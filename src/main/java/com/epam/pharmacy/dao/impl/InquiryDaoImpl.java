package com.epam.pharmacy.dao.impl;

import com.epam.pharmacy.beans.Inquiry;
import com.epam.pharmacy.dao.AbstractDao;
import com.epam.pharmacy.dao.InquiryDao;
import com.epam.pharmacy.exceptions.DaoException;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class InquiryDaoImpl extends AbstractDao<Inquiry> implements InquiryDao {

    private static final String INSERT_INQUIRY = "INSERT INTO `inquiry` (`doctor_id`, `recipe_id`, `days`) VALUES (?, ?, ?)";
    private static final String INQUIRY_CREATION_ERROR = "Inquiry creation error";

    /*package-private */ InquiryDaoImpl(Connection connection) {
        super(connection);
    }

    @Override
    public Integer create(Inquiry entity) throws DaoException {
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            statement = connection.prepareStatement(INSERT_INQUIRY, Statement.RETURN_GENERATED_KEYS);
            statement.setInt(1, entity.getDoctorId());
            statement.setInt(2, entity.getRecipeId());
            statement.setInt(3, entity.getDays());
            statement.executeUpdate();
            resultSet = statement.getGeneratedKeys();
            if (resultSet.next()) {
                return resultSet.getInt(1);
            } else {
                throw new DaoException(INQUIRY_CREATION_ERROR);
            }
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            closeResultSet(resultSet);
            closePreparedStatement(statement);
        }
    }

    @Override
    public Inquiry read(Integer identity) {
        return null;
    }

    @Override
    public void update(Inquiry entity) throws DaoException {

    }

    @Override
    public void delete(Inquiry entity) {

    }
}
