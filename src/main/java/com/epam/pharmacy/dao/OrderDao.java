package com.epam.pharmacy.dao;

import com.epam.pharmacy.beans.Medicine;
import com.epam.pharmacy.beans.Order;
import com.epam.pharmacy.exceptions.DaoException;

public interface OrderDao extends Dao<Order> {

    void updateOrderMedicineListTable(Order order, int order_id) throws DaoException;
}
