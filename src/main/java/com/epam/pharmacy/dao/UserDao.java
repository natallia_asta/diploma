package com.epam.pharmacy.dao;

import com.epam.pharmacy.beans.User;
import com.epam.pharmacy.exceptions.DaoException;

import java.sql.Connection;
import java.util.Optional;

public interface UserDao extends Dao<User> {

    Optional<User> findUserByLoginAndPassword(String login, String password) throws DaoException;

    boolean isLoginExist(String login) throws DaoException;

    int createRequestToDoctor(int clientId, int recipeId, String days) throws DaoException;
}
