package com.epam.pharmacy.dao;

import com.epam.pharmacy.beans.Identifiable;
import com.epam.pharmacy.exceptions.DaoException;

public interface Dao<T extends Identifiable> {

    // методы для CRUD операций
    Integer create(T entity) throws DaoException;

    T read(Integer identity);

    void update(T entity) throws DaoException;

    void delete(T entity);

}
