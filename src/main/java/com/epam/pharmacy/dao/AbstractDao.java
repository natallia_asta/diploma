package com.epam.pharmacy.dao;

import com.epam.pharmacy.beans.Identifiable;
import com.epam.pharmacy.builder.Builder;
import com.epam.pharmacy.exceptions.DaoException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * A super class for a classes implementing interface Dao, contains common methods for them.
 *
 * @param <T> a type of objects implementing interface Identifiable
 */
public abstract class AbstractDao<T extends Identifiable> implements Dao<T> {

    /** Constant for storage Logger instance */
    private static final Logger LOG = LogManager.getLogger(AbstractDao.class);

    /** Constant for storage an error message */
    private static final String CAN_T_CLOSE_STATEMENT = "Can't close statement";

    /** Constant for storage an error message */
    private static final String CAN_T_CLOSE_RESULT_SET = "Can't close resultSet";

    /** Variable for storage an instance of a connection */
    protected Connection connection;

    /**
     *  Constructor with parameter.
     *
      * @param connection an instance of a Connection class
     */
    protected  AbstractDao(Connection connection) {
        this.connection = connection;
    }

    /**
     * Returns a List of the objects of the type<T> resulting from the query to the database.
     *
     * @param connection an instance of a Connection class
     * @param query a string containing the SQL-query
     * @param builder an object of the concrete Builder
     * @param params an array of the Strings containing parameters of the query
     * @return a list of the built objects of the type <T> according the query
     * @throws DaoException in the case of the database error
     */
    protected List<T> executeQuery(Connection connection, String query, Builder<T> builder, String... params) throws DaoException {
        List<T> entities = new ArrayList<>();
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            statement = connection.prepareStatement(query);
            for (int i = 0; i < params.length; i++) {
                statement.setString(i + 1, params[i]);
            }
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                T entity = builder.build(resultSet);
                entities.add(entity);
            }
            return entities;

        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            closeResultSet(resultSet);
            closePreparedStatement(statement);
        }
    }

    /**
     * Returns an Optional object of the type<T> resulting from the query to the database.
     *
     * @param connection an instance of a Connection class
     * @param query a string containing the SQL-query
     * @param builder an object of the concrete Builder
     * @param params an array of the Strings containing parameters of the query
     * @return an Optional object of the type <T> according the query
     * @throws DaoException in the case of the database error
     */
    protected Optional<T> executeForSingleResult(Connection connection, String query, Builder<T> builder, String... params) throws DaoException {
        List<T> entities = executeQuery(connection, query, builder, params);
        if (entities.size() > 0) {
            return Optional.ofNullable(entities.get(0));
        } else {
            return Optional.empty();
        }
    }

    /**
     * Releases this PreparedStatement object's database and JDBC resources immediately instead of waiting for
     * this to happen when it is automatically closed.
     *
     * @param statement an instance of the PreparedStatement class
     */
    protected void closePreparedStatement(PreparedStatement statement) {
        try {
            if (statement != null) {
                statement.close();
            }
        } catch (SQLException e) {
            LOG.error(CAN_T_CLOSE_STATEMENT, e.getMessage());
        }
    }

    /**
     * Releases this object's  database and JDBC resources immediately instead of waiting for
     * this to happen when it is automatically closed.
     *
     * @param resultSet an instance of the ResultSet class
     */
    protected void closeResultSet(ResultSet resultSet) {
        try {
            if (resultSet != null) {
                resultSet.close();
            }
        } catch (SQLException e) {
            LOG.error(CAN_T_CLOSE_RESULT_SET, e.getMessage());
        }
    }

}

