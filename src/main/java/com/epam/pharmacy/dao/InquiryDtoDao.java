package com.epam.pharmacy.dao;

import com.epam.pharmacy.dto.InquiryDto;
import com.epam.pharmacy.exceptions.DaoException;

import java.util.List;

public interface InquiryDtoDao extends Dao<InquiryDto> {

    List<InquiryDto> getInquiryRecipeDataByDoctorId(int doctorId) throws DaoException;

    List<InquiryDto> getInquiryRecipeDataByClientId(int clientId) throws DaoException;
}
