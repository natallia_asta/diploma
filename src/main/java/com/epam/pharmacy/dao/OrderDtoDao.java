package com.epam.pharmacy.dao;

import com.epam.pharmacy.dto.OrderDto;
import com.epam.pharmacy.exceptions.DaoException;

import java.util.List;

public interface OrderDtoDao extends Dao<OrderDto> {

    List<OrderDto> getOrderDataById(int id) throws DaoException;

}
