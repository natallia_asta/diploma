package com.epam.pharmacy.dao;

import com.epam.pharmacy.dto.RecipeDto;
import com.epam.pharmacy.exceptions.DaoException;

import java.util.List;
import java.util.Optional;

public interface RecipeDtoDao extends Dao<RecipeDto> {

    Optional<RecipeDto> getRecipeDataByParameters
            (String recipeNumber, int userId, String tradeName, String amount)
            throws DaoException;

    List<RecipeDto> getRecipeDataByClientId(int clientId) throws DaoException;
}
