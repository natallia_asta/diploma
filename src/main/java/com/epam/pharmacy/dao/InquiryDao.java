package com.epam.pharmacy.dao;

import com.epam.pharmacy.beans.Inquiry;

public interface InquiryDao extends Dao<Inquiry> {

}
