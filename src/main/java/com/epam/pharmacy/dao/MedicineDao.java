package com.epam.pharmacy.dao;

import com.epam.pharmacy.beans.Medicine;
import com.epam.pharmacy.exceptions.DaoException;

import java.util.List;

public interface MedicineDao extends Dao<Medicine> {

    List<Medicine> findMedicineByTradeName(String name) throws DaoException;

    List<Medicine> findMedicineByContainingInn(String inn) throws DaoException;

    List<Medicine> getAll() throws DaoException;

    void updateAmount(int medicineId, int amount) throws DaoException;

}
