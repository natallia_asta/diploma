package com.epam.pharmacy.dto;

import com.epam.pharmacy.beans.Identifiable;
import com.epam.pharmacy.beans.Order;

import java.time.LocalDate;

public class OrderDto extends Order implements Identifiable {

    private String tradeName;
    private int amount;

    public OrderDto(int number, LocalDate date, float totalPrice, boolean isOpen, String tradeName, int amount){
        super(number, date, totalPrice, isOpen);
        this.tradeName = tradeName;
        this.amount = amount;
    }

    public int getAmount() {
        return amount;
    }

    public String getTradeName() {
        return tradeName;
    }

    public int getNumber() {
        return super.getId();
    }

    public boolean getIsOpen() {
        return super.isOpen();
    }

    @Override
    public LocalDate getDate() {
        return super.getDate();
    }

    @Override
    public float getTotalPrice() {
        return super.getTotalPrice();
    }

    @Override
    public int getId() {
        return super.getId();
    }

}
