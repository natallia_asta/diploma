package com.epam.pharmacy.dto;

import com.epam.pharmacy.beans.Identifiable;
import com.epam.pharmacy.beans.Recipe;
import com.epam.pharmacy.beans.RecipeStatus;

import java.time.LocalDate;

public class RecipeDto extends Recipe implements Identifiable {

    private int id;
    private String tradeName;
    private String medicineInn;
    private int amount;
    private int doctorId;
    private int notRedeemed;

    public RecipeDto(int recipeId, int clientId, int recipeNumber, LocalDate validityDate, String tradeName, int amount,
                     int doctorId, RecipeStatus status, String medicineInn, int notRedeemed) {
        super (recipeId, clientId, recipeNumber, validityDate, doctorId, status);
        this.tradeName = tradeName;
        this.amount = amount;
        this.medicineInn = medicineInn;
        this.notRedeemed = notRedeemed;

    }

    /*package-private*/ RecipeDto(int recipeId, String tradeName, int amount, int notRedeemed) {
        super(recipeId);
        this.tradeName = tradeName;
        this.amount = amount;
        this.notRedeemed = notRedeemed;
    }

    public RecipeDto(int recipeId, int recipeNumber, int clientId, LocalDate validityDate, String tradeName, int amount, int notRedeemed) {
        super(recipeId, recipeNumber, clientId, validityDate);
        this.tradeName = tradeName;
        this.notRedeemed =  notRedeemed;
    }

    public int getRecipeId(){
        return super.getRecipeId();
    }

    public int getRecipeNumber(){
        return super.getNumber();
    }

    public RecipeStatus getRecipeStatus(){
        return super.getStatus();
    }

    public String getTradeName() {
        return tradeName;
    }

    public String getMedicineInn() {
        return medicineInn;
    }

    public int getNotRedeemed() {
        return notRedeemed;
    }

    public int getAmount() {
        return amount;
    }


    @Override
    public int getId() {
        return super.getId();
    }

}
