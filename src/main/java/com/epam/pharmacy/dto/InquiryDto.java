package com.epam.pharmacy.dto;

import com.epam.pharmacy.beans.Identifiable;
import com.epam.pharmacy.beans.InquiryStatus;
import com.epam.pharmacy.beans.RecipeStatus;

import java.time.LocalDate;
import java.util.Objects;

public class InquiryDto extends RecipeDto implements Identifiable {

    private int inquiryId;
    private int days;
    private InquiryStatus inquiryStatus;

    public InquiryDto(int recipeId, int inquiryId, int recipeNumber, int clientId, LocalDate validityDate, String tradeName,
                      int amount, int doctorId, RecipeStatus status, String medicineInn, int notRedeemed,
                      int days, InquiryStatus inquiryStatus) {
        super(recipeId, clientId, recipeNumber, validityDate, tradeName, amount, doctorId, status, medicineInn, notRedeemed);
        this.inquiryId = inquiryId;
        this.days = days;
        this.inquiryStatus = inquiryStatus;
    }

    public InquiryDto(int recipeId, int inquiryId, int recipeNumber, int clientId, LocalDate validityDate, String tradeName, int amount, int notRedeemed, int days, InquiryStatus inquiryStatus) {
        super(recipeId, recipeNumber, clientId, validityDate, tradeName, amount, notRedeemed);
        this.inquiryId = inquiryId;
        this.days = days;
        this.inquiryStatus = inquiryStatus;
    }

    public int getDays() {
        return days;
    }

    public void setDays(int days) {
        this.days = days;
    }

    public InquiryStatus getInquiryStatus() {
        return inquiryStatus;
    }

    @Override
    public String getTradeName() {
        return super.getTradeName();
    }

    @Override
    public int getAmount() {
        return super.getAmount();
    }

    @Override
    public int getId() {
        return inquiryId;
    }

    @Override
    public int getNumber() {
        return super.getNumber();
    }

    @Override
    public int getNotRedeemed() {
        return super.getNotRedeemed();
    }

    @Override
    public int getRecipeId() {
        return super.getRecipeId();
    }

    public void setInquiryStatus(InquiryStatus inquiryStatus) {
        this.inquiryStatus = inquiryStatus;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        InquiryDto that = (InquiryDto) o;
        return days == that.days &&
                inquiryStatus == that.inquiryStatus;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), days, inquiryStatus);
    }
}
