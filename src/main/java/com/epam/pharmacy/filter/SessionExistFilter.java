package com.epam.pharmacy.filter;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.StringTokenizer;

import static com.epam.pharmacy.Constants.KEY_USER;

public class SessionExistFilter implements Filter {

    private static final Logger LOG = LogManager.getLogger(SessionExistFilter.class);
    private ArrayList<String> urlList;

    @Override
    public void init(FilterConfig config) throws ServletException {
        String urls = config.getInitParameter("avoid-urls");
        StringTokenizer token = new StringTokenizer(urls, ",");
        urlList = new ArrayList<>();
        while (token.hasMoreTokens()) {
            urlList.add(token.nextToken());
        }
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse,
                         FilterChain chain) throws IOException, ServletException {

        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        String url = request.getServletPath();
        LOG.debug("request " + request.getContextPath() + " " + request.getServletPath());
        if (urlList.contains(url) || url.endsWith(".css")|| url.endsWith(".jpg")) {
            chain.doFilter(servletRequest, servletResponse);
        } else {
            HttpSession session = request.getSession(false);
            if (session != null && session.getAttribute(KEY_USER) != null) {
                chain.doFilter(servletRequest, servletResponse);
            } else {
                response.sendRedirect(request.getContextPath());
            }
        }

    }

    @Override
    public void destroy() {

    }
}
