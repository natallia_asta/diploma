package com.epam.pharmacy.servlet;


import com.epam.pharmacy.command.Command;
import com.epam.pharmacy.command.CommandFactory;
import com.epam.pharmacy.exceptions.CommandException;
import com.epam.pharmacy.exceptions.ServiceException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static com.epam.pharmacy.Constants.ERROR_JSP;
import static com.epam.pharmacy.Constants.KEY_ERROR_MESSAGE;
import static com.epam.pharmacy.Constants.PARAMETER_COMMAND;
import static com.epam.pharmacy.Constants.VALUE_APPLICATION_ERROR;

@WebServlet(name = "Servlet", urlPatterns = {"/Servlet", "/login", "/registration", "/changeLanguage", "/pharmacist"})
public class Servlet extends HttpServlet {

    private static final Logger LOG = LogManager.getLogger(Servlet.class);

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        process(request, response);
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        process(request, response);
    }

    public void process(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String command = request.getParameter(PARAMETER_COMMAND);
        String page = null;
        try {
            Command action = CommandFactory.create(command);
            action.init();
            page = action.execute(request, response);
            action.done();
        } catch (CommandException | ServiceException e) {
            LOG.error(VALUE_APPLICATION_ERROR + e.getMessage(), e);
            request.setAttribute(KEY_ERROR_MESSAGE, VALUE_APPLICATION_ERROR);
            RequestDispatcher dispatcher = request.getRequestDispatcher(ERROR_JSP);
            dispatcher.forward(request, response);
        }

        if (page != null) {
            RequestDispatcher dispatcher = request.getRequestDispatcher(page);
            dispatcher.forward(request, response);
        }
    }
}
