package com.epam.pharmacy.command;

import com.epam.pharmacy.beans.Order;
import com.epam.pharmacy.beans.SessionUser;
import com.epam.pharmacy.exceptions.CommandException;
import com.epam.pharmacy.service.OrderService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import static com.epam.pharmacy.Constants.KEY_USER;
import static com.epam.pharmacy.Constants.ORDER_JSP;

public class DeleteFromOrderCommand implements Command {

    private static final String PARAMETER_CHECKBOX = "checkbox";

    @Override
    public void init() {
    }

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        OrderService service = new OrderService();
        HttpSession session = request.getSession(false);
        SessionUser user = (SessionUser) session.getAttribute(KEY_USER);
        Order order = user.getOrder();
        String[] selectedCheckbox = request.getParameterValues(PARAMETER_CHECKBOX);
        service.removeSelectedItems(order, selectedCheckbox);
        service.calculateAndSetOrderTotalPrice(order);
        return ORDER_JSP;
    }

    @Override
    public void done() {
    }

}


