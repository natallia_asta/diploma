package com.epam.pharmacy.command.db;

import com.epam.pharmacy.beans.RecipeStatus;
import com.epam.pharmacy.beans.SessionUser;
import com.epam.pharmacy.dto.RecipeDto;
import com.epam.pharmacy.exceptions.ServiceException;
import com.epam.pharmacy.service.InquiryService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

import static com.epam.pharmacy.Constants.KEY_ERROR_MESSAGE;
import static com.epam.pharmacy.Constants.KEY_MESSAGE;
import static com.epam.pharmacy.Constants.KEY_USER;
import static com.epam.pharmacy.Constants.USER_PAGE_JSP;
import static com.epam.pharmacy.Constants.WRITE_DOCTOR_JSP;

public class WriteToDoctorCommand extends AbstractDBCommand {

    private static final String VALUE_WRONG_RECIPE_DATA = "error.invalidRecipeData";
    private static final String VALUE_INQUIRY_PLACED = "message.inquiryPlaced";
    private static final String KEY_NUMBER = "registeredNumber";
    private static final String PARAMETER_NUMBER = "number";
    private static final String PARAMETER_DAYS = "days";
    private InquiryService inquiryService;

    public WriteToDoctorCommand() {
        inquiryService = new InquiryService();
    }

    @Override
    public void init() {
        inquiryService.setDaoFactory(daoFactory);
    }

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws ServiceException, IOException {
        HttpSession session = request.getSession(false);
        SessionUser user = (SessionUser) session.getAttribute(KEY_USER);

        String recipeNumber = request.getParameter(PARAMETER_NUMBER);
        int number = Integer.parseInt(recipeNumber);
        List<RecipeDto> recipeDto = user.getRecipeDto();
        Integer recipeId = null;
        Integer doctorId = null;
        RecipeStatus status = null;
        for (RecipeDto dto : recipeDto) {
            int currentRecipeNumber = dto.getNumber();
            if (currentRecipeNumber == number) {
                recipeId = dto.getId();
                doctorId = dto.getDoctorId();
                status = dto.getStatus();
                break;
            }
        }

        if (recipeId != null && !status.equals(RecipeStatus.CLOSED)) {
            int days = Integer.parseInt(request.getParameter(PARAMETER_DAYS));
            int inquiryNumber = inquiryService.createInquiry(doctorId, recipeId, days);
            session.setAttribute(KEY_MESSAGE, VALUE_INQUIRY_PLACED);
            session.setAttribute(KEY_NUMBER, inquiryNumber);
            response.sendRedirect(USER_PAGE_JSP);
            return null;
        } else {
            request.setAttribute(KEY_ERROR_MESSAGE, VALUE_WRONG_RECIPE_DATA);
            return WRITE_DOCTOR_JSP;
        }
    }
}
