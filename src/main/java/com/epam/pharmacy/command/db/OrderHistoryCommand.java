package com.epam.pharmacy.command.db;

import com.epam.pharmacy.beans.SessionUser;
import com.epam.pharmacy.dto.OrderDto;
import com.epam.pharmacy.exceptions.ServiceException;
import com.epam.pharmacy.service.OrderService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;

import static com.epam.pharmacy.Constants.KEY_MESSAGE;
import static com.epam.pharmacy.Constants.KEY_USER;
import static com.epam.pharmacy.Constants.ORDER_HISTORY_JSP;
import static com.epam.pharmacy.Constants.USER_PAGE_JSP;

public class OrderHistoryCommand extends AbstractDBCommand {

    private static final String VALUE_NO_ITEMS_TO_DISPLAY = "message.noItemsToDisplay";
    private static final String KEY_CURRENT_PAGE = "currentPage";
    private static final Integer VALUE_CURRENT_PAGE = 1;

    private final OrderService orderService;

    public OrderHistoryCommand() {
        orderService = new OrderService();
    }

    @Override
    public void init() {
        orderService.setDaoFactory(daoFactory);
    }

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws ServiceException {
        HttpSession session = request.getSession(false);
        SessionUser user = (SessionUser) session.getAttribute(KEY_USER);
        List<OrderDto> orderDto = orderService.getOrderDataByClientId(user.getId());
        user.setOrderDto(orderDto);
        request.setAttribute(KEY_CURRENT_PAGE, VALUE_CURRENT_PAGE);
        if (orderDto.size() == 0) {
            request.setAttribute(KEY_MESSAGE, VALUE_NO_ITEMS_TO_DISPLAY);
            return USER_PAGE_JSP;
        }
        return ORDER_HISTORY_JSP;
    }

}
