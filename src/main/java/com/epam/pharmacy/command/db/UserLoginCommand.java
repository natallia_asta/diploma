package com.epam.pharmacy.command.db;


import com.epam.pharmacy.beans.Medicine;
import com.epam.pharmacy.beans.SessionUser;
import com.epam.pharmacy.beans.User;
import com.epam.pharmacy.beans.UserRole;
import com.epam.pharmacy.dto.InquiryDto;
import com.epam.pharmacy.exceptions.ServiceException;
import com.epam.pharmacy.service.InquiryService;
import com.epam.pharmacy.service.MedicineService;
import com.epam.pharmacy.service.OrderService;
import com.epam.pharmacy.service.RecipeService;
import com.epam.pharmacy.service.UserService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Optional;

import static com.epam.pharmacy.Constants.DOCTOR_PAGE_JSP;
import static com.epam.pharmacy.Constants.ERROR_JSP;
import static com.epam.pharmacy.Constants.INDEX_JSP;
import static com.epam.pharmacy.Constants.KEY_ERROR_MESSAGE;
import static com.epam.pharmacy.Constants.KEY_MESSAGE;
import static com.epam.pharmacy.Constants.KEY_USER;
import static com.epam.pharmacy.Constants.PHARMACIST_PAGE_JSP;
import static com.epam.pharmacy.Constants.USER_PAGE_JSP;
import static com.epam.pharmacy.Constants.VALUE_APPLICATION_ERROR;

public class UserLoginCommand extends AbstractDBCommand {

    private static final String VALUE_INVALID_LOGIN_CREDENTIALS = "error.invalidCredentials";
    private static final String LOGIN = "login";
    private static final String PASSWORD = "password";
    private static final String MEDICINE_LIST = "medicineList";

    private final UserService userService;
    private final RecipeService recipeService;
    private final InquiryService inquiryService;
    private final MedicineService medicineService;
    private final OrderService orderService;

    public UserLoginCommand() {
        userService = new UserService();
        recipeService = new RecipeService();
        inquiryService = new InquiryService();
        medicineService = new MedicineService();
        orderService = new OrderService();
    }

    @Override
    public void init() {
        userService.setDaoFactory(daoFactory);
        recipeService.setDaoFactory(daoFactory);
        inquiryService.setDaoFactory(daoFactory);
        medicineService.setDaoFactory(daoFactory);
        orderService.setDaoFactory(daoFactory);
    }

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws ServiceException {
        String login = request.getParameter(LOGIN);
        String password = request.getParameter(PASSWORD);
        Optional<User> optionalUser;
        optionalUser = userService.login(login, password);
        if (optionalUser.isPresent()) {
            User user = optionalUser.get();
            return choiceTargetPage(request, new SessionUser(user));
        } else {
            request.setAttribute(KEY_ERROR_MESSAGE, VALUE_INVALID_LOGIN_CREDENTIALS);
            return INDEX_JSP;
        }
    }

    private String choiceTargetPage(HttpServletRequest request, SessionUser user) throws ServiceException {
        HttpSession session = request.getSession();
        session.setAttribute(KEY_USER, user);
        UserRole role = user.getRole();
        switch (role) {
            case CLIENT:
                return USER_PAGE_JSP;
            case PHARMACIST:
                List<Medicine> medicineList = medicineService.getAllMedicineList();
                request.setAttribute(MEDICINE_LIST, medicineList);
                return PHARMACIST_PAGE_JSP;
            case DOCTOR:
                List<InquiryDto> inquiryDto = inquiryService.getDataFromInquiryAndRecipeByDoctorId(user.getId());
                user.setInquiryDto(inquiryDto);
                return DOCTOR_PAGE_JSP;
            default:
                request.setAttribute(KEY_MESSAGE, VALUE_APPLICATION_ERROR);
                return ERROR_JSP;
        }
    }
}
