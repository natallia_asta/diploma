package com.epam.pharmacy.command.db;

import com.epam.pharmacy.beans.SessionUser;
import com.epam.pharmacy.dto.InquiryDto;
import com.epam.pharmacy.exceptions.ServiceException;
import com.epam.pharmacy.service.InquiryService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;

import static com.epam.pharmacy.Constants.INQUIRY_HISTORY_JSP;
import static com.epam.pharmacy.Constants.KEY_MESSAGE;
import static com.epam.pharmacy.Constants.KEY_USER;
import static com.epam.pharmacy.Constants.USER_PAGE_JSP;

public class MyInquiriesCommand extends AbstractDBCommand {

    public static final String
            NO_ITEMS_TO_DISPLAY_YET = "No items to display yet.";
    private final InquiryService inquiryService;

    public MyInquiriesCommand() {
        inquiryService = new InquiryService();
    }

    @Override
    public void init() {
        inquiryService.setDaoFactory(daoFactory);
    }

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws ServiceException {
        HttpSession session = request.getSession(false);
        SessionUser user = (SessionUser) session.getAttribute(KEY_USER);
        List<InquiryDto> inquiryDto = inquiryService.getDataFromInquiryAndRecipeByClientId(user.getId());
        user.setInquiryDto(inquiryDto);
        if (inquiryDto.size() == 0) {
            request.setAttribute(KEY_MESSAGE, NO_ITEMS_TO_DISPLAY_YET);
            return USER_PAGE_JSP;
        }
        return INQUIRY_HISTORY_JSP;
    }

}
