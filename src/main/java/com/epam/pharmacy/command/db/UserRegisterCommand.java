package com.epam.pharmacy.command.db;

import com.epam.pharmacy.beans.User;
import com.epam.pharmacy.exceptions.InvalidUserDataException;
import com.epam.pharmacy.exceptions.ServiceException;
import com.epam.pharmacy.service.UserService;
import com.epam.pharmacy.validator.UserRegistrationDataValidator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static com.epam.pharmacy.Constants.INDEX_JSP;
import static com.epam.pharmacy.Constants.KEY_ERROR_MESSAGE;
import static com.epam.pharmacy.Constants.REGISTRATION_JSP;

public class UserRegisterCommand extends AbstractDBCommand {

    private static final String KEY_INVITE_MESSAGE = "inviteMessage";
    private static final String KEY_USER_DATA = "userData";
    private static final String PARAMETER_NAME = "name";
    private static final String PARAMETER_SURNAME = "surname";
    private static final String PARAMETER_LOGIN = "login";
    private static final String PARAMETER_TELEPHONE = "telephone";
    private static final String PARAMETER_PASSWORD = "password";
    private static final String PARAMETER_PASSWORD_REPEAT = "password_repeat";
    private static final String VALUE_SUCCESSFUL = "message.SuccessfullyRegistered";

    private final UserService userService;

    public UserRegisterCommand() {
        userService = new UserService();
    }

    @Override
    public void init() {
        userService.setDaoFactory(daoFactory);
    }

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws ServiceException {
        User user = createUserFromRequest(request);
        String passwordRepeat = request.getParameter(PARAMETER_PASSWORD_REPEAT);
        UserRegistrationDataValidator validator = new UserRegistrationDataValidator(userService);
        String errorMessage = null;
        try {
            validator.validate(user, passwordRepeat);
        } catch (InvalidUserDataException exception) {
            // the errorMessage contains a clear explanation for the user why the input is not valid
            errorMessage = exception.getMessage();
        }

        if (errorMessage == null) {
            int id = userService.register(user);
            user.setId(id);
            request.setAttribute(KEY_INVITE_MESSAGE, VALUE_SUCCESSFUL);
            return INDEX_JSP;
        } else {
            request.setAttribute(KEY_ERROR_MESSAGE, errorMessage);
            request.setAttribute(KEY_USER_DATA, user);
            return REGISTRATION_JSP;
        }
    }

    private User createUserFromRequest(HttpServletRequest request) {
        String name = request.getParameter(PARAMETER_NAME);
        String surname = request.getParameter(PARAMETER_SURNAME);
        String login = request.getParameter(PARAMETER_LOGIN);
        String phone = request.getParameter(PARAMETER_TELEPHONE);
        String password = request.getParameter(PARAMETER_PASSWORD);

        User user = new User();
        user.setName(name);
        user.setSurname(surname);
        user.setLogin(login);
        user.setPassword(password);
        user.setTelephone(phone);
        return user;
    }
}
