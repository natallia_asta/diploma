package com.epam.pharmacy.command.db;

import com.epam.pharmacy.command.Command;
import com.epam.pharmacy.dao.impl.DaoFactory;
import com.epam.pharmacy.db.ConnectionPool;
import com.epam.pharmacy.exceptions.CommandException;
import com.epam.pharmacy.exceptions.DaoException;

import java.sql.Connection;

/**
 * Super class for classes which implements interface Command and appeals to database.
 */
public abstract class AbstractDBCommand implements Command {

    /** Constant for storage error message. */
    private static final String
            EXCEPTION_IN_DONE_METHOD = "Command generates exception when call done method.";

    /** The value is used for storage a DaoFactory object. */
    protected DaoFactory daoFactory;

    /** Sets the value of a DaoFactory object. */
    public void setDaoFactory(DaoFactory daoFactory) {
        this.daoFactory = daoFactory;
    }

    /**
     * Returns connection to the ConnectionPool then the command executed.
     *
     * @throws CommandException in the case of database error
     */
    @Override
    public void done() throws CommandException {
        try {
            final Connection connection = daoFactory.getConnection();
            ConnectionPool.getInstance().returnConnection(connection);
        } catch (DaoException e) {
            throw new CommandException(EXCEPTION_IN_DONE_METHOD, e);
        } finally {
            daoFactory = null;
        }
    }
}
