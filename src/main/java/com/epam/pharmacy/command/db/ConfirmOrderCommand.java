package com.epam.pharmacy.command.db;

import com.epam.pharmacy.beans.Order;
import com.epam.pharmacy.beans.SessionUser;
import com.epam.pharmacy.exceptions.ServiceException;
import com.epam.pharmacy.service.TransactionalConfirmOrderService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

import static com.epam.pharmacy.Constants.KEY_ERROR_MESSAGE;
import static com.epam.pharmacy.Constants.KEY_MESSAGE;
import static com.epam.pharmacy.Constants.KEY_USER;
import static com.epam.pharmacy.Constants.ORDER_JSP;
import static com.epam.pharmacy.Constants.USER_PAGE_JSP;

public class ConfirmOrderCommand extends AbstractDBCommand {

    private static final String VALUE_NOTHING_SELECTED = "error.nothingSelected";
    private static final String KEY_NUMBER = "registeredNumber";
    private static final String VALUE_ORDER_PLACED = "message.orderPlaced";

    private final TransactionalConfirmOrderService transactionalConfirmOrderService;

    public ConfirmOrderCommand() {
        transactionalConfirmOrderService = new TransactionalConfirmOrderService();
    }

    @Override
    public void init() {
        transactionalConfirmOrderService.setDaoFactory(daoFactory);
    }

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws ServiceException, IOException {
        HttpSession session = request.getSession(false);
        SessionUser user = (SessionUser) session.getAttribute(KEY_USER);
        int userId = user.getId();
        Order order = user.getOrder();
        if (order.getMedicineList().size() != 0) {
            int orderId = transactionalConfirmOrderService.createOrderAndUpdateRecipeAndMedicineData(order, userId);
            clearOrder(order);
            session.setAttribute(KEY_MESSAGE, VALUE_ORDER_PLACED);
            session.setAttribute(KEY_NUMBER, orderId);
            response.sendRedirect(USER_PAGE_JSP);
            return null;
        } else {
            request.setAttribute(KEY_ERROR_MESSAGE, VALUE_NOTHING_SELECTED);
            return ORDER_JSP;
        }
    }

    private void clearOrder(Order order){
        order.getMedicineList().clear();
        order.getMedicineRecipe().clear();
        order.setTotalPrice(0);
    }
}
