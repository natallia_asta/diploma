package com.epam.pharmacy.command.db;

import com.epam.pharmacy.beans.Medicine;
import com.epam.pharmacy.beans.SessionUser;
import com.epam.pharmacy.exceptions.ServiceException;
import com.epam.pharmacy.service.MedicineService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;

import static com.epam.pharmacy.Constants.KEY_MESSAGE;
import static com.epam.pharmacy.Constants.KEY_USER;
import static com.epam.pharmacy.Constants.SEARCH_JSP;
import static com.epam.pharmacy.Constants.SEARCH_RESULT_JSP;

public class SearchDragCommand extends AbstractDBCommand {

    private static final String PARAMETER_SEARCH = "search";
    private static final String VALUE_NO_RESULTS_FOR_YOUR_SEARCH = "message.noResults";

    private final MedicineService medicineService;

    public SearchDragCommand() {
        medicineService = new MedicineService();
    }

    @Override
    public void init() {
        medicineService.setDaoFactory(daoFactory);
    }

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws ServiceException {
        String search = request.getParameter(PARAMETER_SEARCH);
        List<Medicine> foundMedicineList = medicineService.search(search);
        if (foundMedicineList.size() > 0) {
            HttpSession session = request.getSession(false);
            SessionUser user = (SessionUser) session.getAttribute(KEY_USER);
            List<Medicine> foundBySearch = user.getFoundBySearch();
            // clearing results of previous search:
            foundBySearch.clear();
            foundBySearch.addAll(foundMedicineList);
            return SEARCH_RESULT_JSP;
        } else {
            request.setAttribute(KEY_MESSAGE, VALUE_NO_RESULTS_FOR_YOUR_SEARCH);
            return SEARCH_JSP;
        }
    }
}

