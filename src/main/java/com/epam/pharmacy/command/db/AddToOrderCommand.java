package com.epam.pharmacy.command.db;

import com.epam.pharmacy.beans.Medicine;
import com.epam.pharmacy.beans.Order;
import com.epam.pharmacy.beans.Recipe;
import com.epam.pharmacy.beans.SessionUser;
import com.epam.pharmacy.exceptions.InvalidRecipeNumberException;
import com.epam.pharmacy.exceptions.ServiceException;
import com.epam.pharmacy.service.OrderService;
import com.epam.pharmacy.service.RecipeService;
import com.epam.pharmacy.validator.RecipeNumberValidator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Map;

import static com.epam.pharmacy.Constants.KEY_ERROR_MESSAGE;
import static com.epam.pharmacy.Constants.KEY_MESSAGE;
import static com.epam.pharmacy.Constants.KEY_USER;
import static com.epam.pharmacy.Constants.SEARCH_RESULT_JSP;

public class AddToOrderCommand extends AbstractDBCommand {

    private static final String PARAMETER_CHECKBOX = "checkbox";
    private static final String PARAMETER_AMOUNT = "amount";
    private static final String PARAMETER_RECIPE_NUMBER = "recipe_number";
    private static final String VALUE_ADDED_TO_ORDER = "message.addedToOrder";
    private static final String VALUE_NOTHING_SELECTED = "error.nothingSelected";
    private static final String VALUE_ALREADY_ADDED = "error.alreadyAdded";
    private final OrderService orderService;
    private final RecipeService recipeService;

    public AddToOrderCommand() {
        orderService = new OrderService();
        recipeService = new RecipeService();
    }

    @Override
    public void init() {
        orderService.setDaoFactory(daoFactory);
        recipeService.setDaoFactory(daoFactory);
    }

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws ServiceException {
        final String[] selectedCheckbox = request.getParameterValues(PARAMETER_CHECKBOX);
        final String[] amounts = request.getParameterValues(PARAMETER_AMOUNT);
        String message = null;

        if (selectedCheckbox != null) {
            final HttpSession session = request.getSession(false);
            final String recipeNumber = request.getParameter(PARAMETER_RECIPE_NUMBER);

            final SessionUser user = (SessionUser) session.getAttribute(KEY_USER);
            final int userId = user.getId();
            final List<Medicine> foundBySearch = user.getFoundBySearch();
            final Order order = user.getOrder();
            final List<Medicine> orderMedicineList = order.getMedicineList();

            for (String line : selectedCheckbox) {
                final int checkboxNum = Integer.parseInt(line);
                Medicine medicine = foundBySearch.get(checkboxNum);
                String amountString = amounts[checkboxNum];
                medicine.setAmount(Integer.parseInt(amountString));
                if (medicine.isRecipeRequired()) {
                    message = checkRecipeAndAddToOrder(recipeNumber, userId, order, medicine);
                } else {
                    orderMedicineList.add(medicine);
                }
            }

            if (message == null) {
                float totalPrice = orderService.calculateAndSetOrderTotalPrice(order);
                order.setTotalPrice(totalPrice);
                message = VALUE_ADDED_TO_ORDER;
                request.setAttribute(KEY_MESSAGE, message);
            } else {
                request.setAttribute(KEY_ERROR_MESSAGE, message);
            }
        } else {
            message = VALUE_NOTHING_SELECTED;
            request.setAttribute(KEY_MESSAGE, message);
        }
        return SEARCH_RESULT_JSP;
    }

    private String checkRecipeAndAddToOrder(String recipeNumber, int userId, Order order, Medicine medicine) throws ServiceException {
        String message = null;
        String tradeName = medicine.getTradeName();
        String amountString = String.valueOf(medicine.getAmount());
        Recipe recipe = null;
        try {
            RecipeNumberValidator validator = new RecipeNumberValidator(recipeService);
            recipe = validator.validate(recipeNumber, userId, tradeName, amountString);
        } catch (InvalidRecipeNumberException e) {
            // the message contains a clear explanation for the user why the recipe is not valid
            message = e.getMessage();
        }
        if (message == null) {
            message = add(order, medicine, recipe);
        }
        return message;
    }

    private String add(Order order, Medicine medicine, Recipe recipe) {
        String message = null;
        Map<Medicine, Recipe> medicineRecipe  = order.getMedicineRecipe();
        List<Medicine> medicineList = order.getMedicineList();
        boolean isContain = medicineRecipe.containsKey(medicine);
        if (!isContain) {
            medicineList.add(medicine);
            medicineRecipe.put(medicine, recipe);
        } else {
            message = VALUE_ALREADY_ADDED;
        }
        return message;
    }
}

