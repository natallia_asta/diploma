package com.epam.pharmacy.command.db;

import com.epam.pharmacy.beans.SessionUser;
import com.epam.pharmacy.dto.RecipeDto;
import com.epam.pharmacy.exceptions.ServiceException;
import com.epam.pharmacy.service.RecipeService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;

import static com.epam.pharmacy.Constants.KEY_MESSAGE;
import static com.epam.pharmacy.Constants.KEY_USER;
import static com.epam.pharmacy.Constants.RECIPE_HISTORY_JSP;
import static com.epam.pharmacy.Constants.USER_PAGE_JSP;
import static com.epam.pharmacy.Constants.VALUE_NO_ITEMS_TO_DISPLAY;

public class MyRecipesCommand extends AbstractDBCommand {

    private final RecipeService recipeService;

    public MyRecipesCommand() {
        recipeService = new RecipeService();
    }

    @Override
    public void init() {
        recipeService.setDaoFactory(daoFactory);
    }

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws ServiceException {
        HttpSession session = request.getSession(false);
        SessionUser user = (SessionUser) session.getAttribute(KEY_USER);
        recipeService.updateRecipeStatusIfExpired(user.getId());
        List<RecipeDto> recipeDto = recipeService.getMedicineDataFromRecipeByUserId(user.getId());
        user.setRecipeDto(recipeDto);
        if (recipeDto.size() == 0) {
            request.setAttribute(KEY_MESSAGE, VALUE_NO_ITEMS_TO_DISPLAY);
            return USER_PAGE_JSP;
        }
        return RECIPE_HISTORY_JSP;
    }

}
