package com.epam.pharmacy.command;


import com.epam.pharmacy.command.db.AbstractDBCommand;
import com.epam.pharmacy.command.db.AddToOrderCommand;
import com.epam.pharmacy.command.db.ConfirmOrderCommand;
import com.epam.pharmacy.command.db.MyInquiriesCommand;
import com.epam.pharmacy.command.db.MyRecipesCommand;
import com.epam.pharmacy.command.db.OrderHistoryCommand;
import com.epam.pharmacy.command.db.SearchDragCommand;
import com.epam.pharmacy.command.db.UserLoginCommand;
import com.epam.pharmacy.command.db.UserRegisterCommand;
import com.epam.pharmacy.command.db.WriteToDoctorCommand;
import com.epam.pharmacy.dao.impl.DaoFactory;
import com.epam.pharmacy.db.ConnectionPool;
import com.epam.pharmacy.exceptions.CommandException;
import com.epam.pharmacy.exceptions.DaoException;

import java.sql.Connection;

public class CommandFactory {

    private static final String COMMAND_CHANGE_LANGUAGE = "changeLanguage";
    private static final String COMMAND_LOG_IN = "logIn";
    private static final String COMMAND_TO_REGISTER = "toRegister";
    private static final String COMMAND_SEARCH = "search";
    private static final String COMMAND_ADD_TO_ORDER = "addToOrder";
    private static final String COMMAND_LOG_OUT = "logOut";
    private static final String COMMAND_CONFIRM_ORDER = "confirmOrder";
    private static final String COMMAND_DELETE = "delete";
    private static final String COMMAND_NEXT = "next";
    private static final String COMMAND_WRITE_TO_DOCTOR = "writeToDoctor";
    private static final String COMMAND_ORDER_HISTORY = "orderHistory";
    private static final String COMMAND_MY_RECIPES = "myRecipes";
    private static final String COMMAND_MY_INQUIRIES = "myInquiries";
    private static final String UNKNOWN_COMMAND = "Unknown command";
    private static final String CAN_T_ASSIGN_CONNECTION = "Can't assign connection to command ";

    public static Command create(String command) throws CommandException {
        Command commandInstance;
        switch (command) {
            case COMMAND_CHANGE_LANGUAGE:
                commandInstance = new ChangeLanguageCommand();
                break;
            case COMMAND_LOG_IN:
                commandInstance = new UserLoginCommand();
                break;
            case COMMAND_TO_REGISTER:
                commandInstance = new UserRegisterCommand();
                break;
            case COMMAND_SEARCH:
                commandInstance = new SearchDragCommand();
                break;
            case COMMAND_ADD_TO_ORDER:
                commandInstance = new AddToOrderCommand();
                break;
            case COMMAND_LOG_OUT:
                commandInstance = new LogOutCommand();
                break;
            case COMMAND_CONFIRM_ORDER:
                commandInstance = new ConfirmOrderCommand();
                break;
            case COMMAND_DELETE:
                commandInstance = new DeleteFromOrderCommand();
                break;
            case COMMAND_NEXT:
                commandInstance = new NextPageCommand();
                break;
            case COMMAND_WRITE_TO_DOCTOR:
                commandInstance = new WriteToDoctorCommand();
                break;
            case COMMAND_ORDER_HISTORY:
                commandInstance = new OrderHistoryCommand();
                break;
            case COMMAND_MY_RECIPES:
                commandInstance = new MyRecipesCommand();
                break;
            case COMMAND_MY_INQUIRIES:
                commandInstance = new MyInquiriesCommand();
                break;
            default:
                throw new CommandException(UNKNOWN_COMMAND);
        }

        if (commandInstance instanceof AbstractDBCommand) {
            try {
                Connection connection = ConnectionPool.getInstance().getConnection();
                DaoFactory daoFactory = new DaoFactory(connection);
                ((AbstractDBCommand) commandInstance).setDaoFactory(daoFactory);
            } catch (DaoException e) {
                throw new CommandException(CAN_T_ASSIGN_CONNECTION + command, e);
            }
        }

        return commandInstance;
    }
}
