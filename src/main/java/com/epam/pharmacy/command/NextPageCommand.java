package com.epam.pharmacy.command;

import com.epam.pharmacy.exceptions.CommandException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static com.epam.pharmacy.Constants.ORDER_HISTORY_JSP;

public class NextPageCommand implements Command {

    private static final String PARAMETER_PAGE_NUM = "pageNum";
    private static final String KEY_CURRENT_PAGE = "currentPage";

    @Override
    public void init() {

    }

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        String page = request.getParameter(PARAMETER_PAGE_NUM);
        int currentPage;
        try {
            currentPage = Integer.parseInt(page);
        } catch (NumberFormatException ignored) {
            currentPage = 1;
        }
        request.setAttribute(KEY_CURRENT_PAGE, currentPage);
        return ORDER_HISTORY_JSP;
    }

    @Override
    public void done() throws CommandException {

    }
}
