package com.epam.pharmacy.command;


import com.epam.pharmacy.exceptions.CommandException;
import com.epam.pharmacy.exceptions.ServiceException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public interface Command {

    void init();

    String execute(HttpServletRequest request, HttpServletResponse response) throws IOException, CommandException, ServiceException;

    void done() throws CommandException;
}
