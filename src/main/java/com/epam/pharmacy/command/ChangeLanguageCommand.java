package com.epam.pharmacy.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class ChangeLanguageCommand implements Command {

    public static final String ATTRIBUTE_PAGE = "page";

    @Override
    public void init() {
    }

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        HttpSession session = request.getSession(false);
        return (String) session.getAttribute(ATTRIBUTE_PAGE);
    }

    @Override
    public void done() {
    }
}