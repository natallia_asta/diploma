package com.epam.pharmacy.command;

import com.epam.pharmacy.beans.User;
import com.epam.pharmacy.exceptions.CommandException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static com.epam.pharmacy.Constants.INDEX_JSP;
import static com.epam.pharmacy.Constants.KEY_MESSAGE;
import static com.epam.pharmacy.Constants.KEY_USER;

public class LogOutCommand implements Command {

    @Override
    public void init() {
    }

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        User user = (User) request.getSession().getAttribute(KEY_USER);
        request.getSession(false).invalidate();
        return INDEX_JSP;
    }

    @Override
    public void done() {
    }
}
