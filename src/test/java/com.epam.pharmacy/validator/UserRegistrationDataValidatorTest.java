package com.epam.pharmacy.validator;

import com.epam.pharmacy.beans.User;
import com.epam.pharmacy.exceptions.InvalidUserDataException;
import com.epam.pharmacy.exceptions.ServiceException;
import com.epam.pharmacy.service.UserService;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class UserRegistrationDataValidatorTest {

    private UserRegistrationDataValidator validator = new UserRegistrationDataValidator(null);

    private final User invalidDataUser = new User(
            "111", "222", "", "123456", "6668800");
    private final User validUser = new User(
            "Maria", "Petrova", "111", "123456", "+375(29)666-88-00");
    private final String passwordRepeat = "123456";
    private final String falsePasswordRepeat = "12345";

    @BeforeTest
    public void setup() throws ServiceException {
        UserService userService = mock(UserService.class);
        when(userService.isLoginAlreadyExist(anyString())).thenReturn(false);

        validator.setUserService(userService);
    }

    @Test
    public void shouldReturnNothingThenUserDataValid() throws InvalidUserDataException, ServiceException {
        validator.validate(validUser, passwordRepeat);
    }

    @Test(expectedExceptions = InvalidUserDataException.class)
    public void shouldThrowExceptionThenUserDataNotValid() throws InvalidUserDataException, ServiceException {
        validator.validate(invalidDataUser, passwordRepeat);
    }

    @Test(expectedExceptions = InvalidUserDataException.class)
    public void shouldThrowExceptionThenPasswordRepeatNotEqualsPassword() throws InvalidUserDataException, ServiceException {
        validator.validate(validUser, falsePasswordRepeat);
    }

    @Test(expectedExceptions = InvalidUserDataException.class)
    public void shouldThrowExceptionThenNameIsInvalid() throws InvalidUserDataException {
        validator.nameValidate(invalidDataUser);
    }

    @Test(expectedExceptions = InvalidUserDataException.class)
    public void shouldThrowExceptionThenSurnameIsInvalid() throws InvalidUserDataException {
        validator.surnameValidate(invalidDataUser);
    }

    @Test(expectedExceptions = InvalidUserDataException.class)
    public void shouldThrowExceptionThenPhoneInvalid() throws InvalidUserDataException {
        validator.phoneValidate(invalidDataUser);
    }

    @Test(expectedExceptions = InvalidUserDataException.class)
    public void shouldThrowExceptionThenLoginInvalid() throws InvalidUserDataException, ServiceException {
        validator.loginValidate(invalidDataUser);
    }
}

