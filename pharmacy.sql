-- MySQL dump 10.13  Distrib 5.7.21, for Win64 (x86_64)
--
-- Host: localhost    Database: pharmacy
-- ------------------------------------------------------
-- Server version	5.7.21-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `inquiry`
--

DROP TABLE IF EXISTS `inquiry`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inquiry` (
  `inquiry_id` int(11) NOT NULL AUTO_INCREMENT,
  `doctor_id` int(11) NOT NULL,
  `recipe_id` int(11) NOT NULL,
  `days` int(11) NOT NULL,
  `status` enum('APPROVED','REJECTED','UNDER_CONSIDERATION') DEFAULT 'UNDER_CONSIDERATION',
  PRIMARY KEY (`inquiry_id`),
  KEY `fk_message_to_doctor` (`doctor_id`),
  KEY `fk_message_to_recipe` (`recipe_id`),
  CONSTRAINT `fk_message_to_doctor` FOREIGN KEY (`doctor_id`) REFERENCES `user` (`user_id`),
  CONSTRAINT `fk_message_to_recipe` FOREIGN KEY (`recipe_id`) REFERENCES `recipe` (`recipe_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `inquiry`
--

LOCK TABLES `inquiry` WRITE;
/*!40000 ALTER TABLE `inquiry` DISABLE KEYS */;
INSERT INTO `inquiry` VALUES (1,18,4,1,'UNDER_CONSIDERATION'),(2,18,5,2,'UNDER_CONSIDERATION'),(3,18,8,3,'UNDER_CONSIDERATION'),(4,18,4,10,'UNDER_CONSIDERATION'),(5,18,4,10,'UNDER_CONSIDERATION'),(6,18,5,4,'UNDER_CONSIDERATION'),(7,18,5,3,'UNDER_CONSIDERATION');
/*!40000 ALTER TABLE `inquiry` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `medicine`
--

DROP TABLE IF EXISTS `medicine`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `medicine` (
  `medicine_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Суррогатный первичный ключ, уникальный идентификатор лекарственного средства  в системе.',
  `trade_name` varchar(25) NOT NULL COMMENT 'Торговое название лекарственного средства',
  `medicine_inn` varchar(45) NOT NULL COMMENT 'Международное непатентованное название (International Nonproprietary Name) лекарственного средства - уникальное наименование действующего вещества лекарственного средства, рекомендованное Всемирной организацией здравоохранения (ВОЗ).',
  `price` float NOT NULL COMMENT 'Стоимость единицы лекарственного средства в белорусских рублях.',
  `available_amount` int(11) DEFAULT NULL COMMENT 'Количество единиц лекарственного средства в наличии.',
  `is_need_recipe` tinyint(4) NOT NULL COMMENT 'Указывает, есть ли лекарственное средство в наличии.',
  `form` enum('TABLETS','SYRUP','CREAM','GEL','DROPS','SUPPOSITORIES','AMPOULES') NOT NULL COMMENT 'Форма выпуска лекарственного средства, может принимать одно из значений из перечисления ''TABLETS'', SYRUP'', ''CREAM'', ''GEL'', ''DROPS'', ''SUPPOSITORIES'', ''AMPOULES''.',
  `doze` varchar(15) NOT NULL COMMENT 'Дозировка действующего вещества в единице лекарственного средства.',
  `producer` varchar(45) NOT NULL,
  PRIMARY KEY (`medicine_id`),
  KEY `inn_index` (`medicine_inn`),
  KEY `trade_name_index` (`trade_name`),
  KEY `inn_and_price_index` (`medicine_inn`,`price`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COMMENT='Таблица описывает лекарственные средства, доступные для заказа.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `medicine`
--

LOCK TABLES `medicine` WRITE;
/*!40000 ALTER TABLE `medicine` DISABLE KEYS */;
INSERT INTO `medicine` VALUES (1,'Парацетамол','Paracetamol',0.25,105,0,'TABLETS','500 mg','Борисовский завод медпрепаратов, Беларусь'),(2,'Парацетамол','Paracetamol',0.22,118,0,'TABLETS','500 mg','BYER AG, Германия'),(3,'Ацетилсалициловая кислота','Acetylsalicylic acid',0.25,250,0,'TABLETS','500 mg','Борисовский завод медпрепаратов, Беларусь'),(4,'Аспирин','Acetylsalicylic acid',0.15,250,0,'TABLETS','500 mg','BYER AG, Германия'),(5,'Цитрамон','Paracetamol',0.36,220,0,'TABLETS','500 mg','Борисовский завод медпрепаратов, Беларусь'),(6,'Ибуфен Форте','Ibuprofen',5.36,40,0,'SYRUP','200mg/5ml','Polpharma, Польша'),(7,'Ибуфен','Ibuprofen',5.36,34,0,'SYRUP','100mg/5ml','Polpharma, Польша'),(8,'Ибупрофен','Ibuprofen',5.36,40,0,'TABLETS','200 mg','Борисовский завод медпрепаратов, Беларусь'),(9,'Називин','Oxymetazoline',4.4,10,0,'DROPS','0.5mg/1ml','Merk GMbH, Германия'),(10,'Парацетамол','Paracetamol',0.15,117,0,'TABLETS','200 mg','Борисовский завод медпрепаратов, Беларусь'),(11,'Парацетамол','Paracetamol',0.25,112,0,'SUPPOSITORIES','100 mg','BYER AG, Германия'),(13,'Назол','Oxymetazoline',3.45,10,0,'DROPS','0.5mg/1ml','Белфарма, Беларусь'),(14,'Ибуклин','Paracetamol, Ibuprofen',5.4,7,1,'TABLETS','500mg, 500mg','Биокодекс, Россия'),(15,'Нурофен','Paracetamol, Ibuprofen',6.12,5,0,'TABLETS','200 mg','Рекитт Хелскэр Интернешнл Лтд, Великобритания'),(16,'Ибуклин Юниор','Aceclofenac',7.7,0,1,'TABLETS','200 mg','Биокодекс, Россия'),(17,'Цефтриаксон','Ceftriaxonum',10.2,5,1,'AMPOULES','0.5g/1ampule','Polpharma, Польша'),(18,'Лидокаин','Lidocainum',7.2,8,1,'AMPOULES','5mg/1ampule','Polpharma, Польша');
/*!40000 ALTER TABLE `medicine` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order`
--

DROP TABLE IF EXISTS `order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order` (
  `order_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Суррогатный первичный ключ, уникальный идентификатор заказа в системе.',
  `date` date NOT NULL COMMENT 'Дата создания заказа.',
  `client_id` int(11) NOT NULL COMMENT 'Внешний ключ, указывающий на клиента, выполнившего данный заказ.',
  `total_price` float NOT NULL COMMENT 'Общая стоимость лекарственных средств, включенных в заказ.',
  `pharmacist_id` int(11) NOT NULL DEFAULT '1',
  `is_open` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`order_id`),
  KEY `fk_order_user1_idx` (`client_id`),
  KEY `fk_order_user2_idx` (`pharmacist_id`),
  CONSTRAINT `fk_order_user1` FOREIGN KEY (`client_id`) REFERENCES `user` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_order_user2` FOREIGN KEY (`pharmacist_id`) REFERENCES `user` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=79 DEFAULT CHARSET=utf8 COMMENT='Таблица описывает заказ.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order`
--

LOCK TABLES `order` WRITE;
/*!40000 ALTER TABLE `order` DISABLE KEYS */;
INSERT INTO `order` VALUES (1,'2018-09-17',1,0.84,1,0),(2,'2018-09-17',2,4.4,1,0),(3,'2018-09-17',2,0.25,1,0),(4,'2018-09-17',3,0.8,1,0),(5,'2018-09-17',4,7.7,1,0),(6,'2018-09-17',5,22.8,2,0),(7,'2018-09-17',6,10.9,1,0),(8,'2018-09-12',16,0.25,1,0),(9,'2018-09-17',16,0.25,1,0),(10,'2018-09-17',16,0.25,1,0),(11,'2018-09-18',16,0.25,1,0),(12,'2018-09-18',16,0.25,1,0),(13,'2018-09-18',16,0.25,1,0),(14,'2018-09-18',16,0.25,1,0),(15,'2018-09-18',16,5.65,1,0),(16,'2018-09-19',16,5.4,1,0),(17,'2018-09-19',16,5.65,1,0),(18,'2018-09-19',16,5.65,1,0),(19,'2018-09-19',16,5.65,1,0),(20,'2018-10-01',16,0.5,1,0),(21,'2018-10-01',16,0.5,1,0),(22,'2018-10-01',16,0.5,1,0),(23,'2018-10-01',16,5.36,1,0),(24,'2018-10-01',16,10.2,1,0),(25,'2018-10-01',16,5.4,1,0),(26,'2018-10-02',16,20.4,1,0),(27,'2018-10-03',16,11.45,1,0),(28,'2018-10-03',16,5.9,1,0),(29,'2018-10-03',16,5.58,1,0),(30,'2018-10-03',16,7.2,1,0),(31,'2018-10-03',16,28.8,1,0),(32,'2018-10-03',16,21.6,1,0),(33,'2018-10-04',16,0.5,1,0),(34,'2018-10-04',16,20.4,1,0),(35,'2018-10-05',16,10.76,1,0),(36,'2018-10-05',16,37,1,0),(37,'2018-10-06',16,16.2,1,0),(38,'2018-10-06',16,5.4,1,0),(40,'2018-10-06',16,11.55,1,0),(41,'2018-10-06',16,5.36,1,0),(42,'2018-10-06',16,0,1,0),(43,'2018-10-06',16,0,1,0),(44,'2018-10-06',16,0.25,1,0),(45,'2018-10-06',16,0,1,0),(46,'2018-10-06',16,0,1,0),(47,'2018-10-06',16,0.25,1,0),(48,'2018-10-06',16,0,1,0),(49,'2018-10-06',16,0,1,0),(50,'2018-10-06',16,0,1,0),(51,'2018-10-06',16,0,1,0),(52,'2018-10-06',16,0,1,0),(53,'2018-10-06',16,0.25,1,0),(54,'2018-10-06',16,5.4,1,0),(55,'2018-10-06',16,0.25,1,0),(56,'2018-10-06',16,0.25,1,0),(57,'2018-10-06',16,5.36,1,0),(58,'2018-10-06',16,0.25,1,0),(59,'2018-10-06',16,0.25,1,0),(60,'2018-10-06',16,0.25,1,0),(61,'2018-10-06',16,0.25,1,0),(62,'2018-10-07',16,5.4,1,0),(63,'2018-10-07',16,10.45,1,0),(64,'2018-10-07',16,0.22,1,0),(65,'2018-10-08',16,0.4,1,0),(66,'2018-10-08',16,10.2,1,0),(67,'2018-10-08',16,0.97,1,0),(68,'2018-10-08',16,5.4,1,0),(69,'2018-10-08',16,10.2,1,0),(70,'2018-10-08',18,0.25,1,0),(71,'2018-10-09',16,14.4,1,0),(72,'2018-10-16',16,16.08,1,0),(73,'2018-10-16',16,0.25,1,0),(74,'2018-10-16',23,0.5,1,0),(75,'2018-10-16',23,5.36,1,0),(76,'2018-10-17',16,1.55,1,1),(77,'2018-10-18',16,10.2,1,1),(78,'2018-10-18',16,10.2,1,1);
/*!40000 ALTER TABLE `order` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_medicine_list`
--

DROP TABLE IF EXISTS `order_medicine_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order_medicine_list` (
  `order_id` int(11) NOT NULL COMMENT 'Поле является частью составного первичного ключа и ссылается на заказ, в  котором содержится данный список.',
  `medicine_id` int(11) NOT NULL COMMENT 'Поле является частью составного первичного ключа и ссылается на лекарство, содержащееся в данном списке.',
  `amount` int(11) NOT NULL COMMENT 'Требуемое количество указанного лекарственного средства.',
  PRIMARY KEY (`order_id`,`medicine_id`),
  KEY `fk_order_to_medicine_list_idx` (`order_id`),
  KEY `fk_order_to_medicine_idx` (`medicine_id`),
  CONSTRAINT `fk_order_to_medicine` FOREIGN KEY (`medicine_id`) REFERENCES `medicine` (`medicine_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_order_to_medicine_list` FOREIGN KEY (`order_id`) REFERENCES `order` (`order_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Таблица содержит список лекарственных средств, включенных в заказ. Позволяет избежать соотношения "многие ко многим" между таблицами order и  medicine. Содержит составной первичный ключ (order_id + medicine_id), позволяющий однозначно идентифицировать запись в таблице.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_medicine_list`
--

LOCK TABLES `order_medicine_list` WRITE;
/*!40000 ALTER TABLE `order_medicine_list` DISABLE KEYS */;
INSERT INTO `order_medicine_list` VALUES (1,2,2),(1,3,1),(1,4,1),(2,9,1),(3,3,1),(4,10,2),(4,11,2),(5,16,1),(6,14,1),(6,17,1),(6,18,1),(7,10,1),(7,14,1),(10,1,1),(11,1,1),(12,1,1),(15,1,1),(15,14,1),(16,14,1),(17,1,1),(17,14,1),(18,1,1),(18,14,1),(19,1,1),(19,14,1),(20,1,2),(21,1,2),(22,1,2),(23,7,1),(24,17,1),(25,14,1),(26,17,2),(27,1,1),(27,10,1),(27,11,1),(27,14,2),(28,1,1),(28,11,1),(28,14,1),(29,2,1),(29,7,1),(30,18,1),(31,18,4),(32,18,3),(33,1,1),(34,17,1),(35,7,1),(35,14,1),(36,7,1),(36,17,1),(37,14,3),(38,14,1),(40,1,2),(40,11,1),(40,14,2),(41,7,1),(44,1,1),(47,1,1),(53,1,1),(54,14,1),(55,1,1),(56,1,1),(57,7,1),(58,1,1),(59,1,1),(60,1,1),(61,1,1),(62,14,1),(63,11,1),(63,17,1),(64,2,1),(65,1,1),(65,10,1),(66,17,1),(67,1,3),(67,2,1),(68,14,1),(69,17,1),(70,1,1),(71,18,2),(72,7,3),(73,11,1),(74,11,2),(75,7,1),(76,1,1),(76,10,2),(76,11,4),(77,17,1),(78,17,1);
/*!40000 ALTER TABLE `order_medicine_list` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `recipe`
--

DROP TABLE IF EXISTS `recipe`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `recipe` (
  `recipe_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Идетнификационный номер рецепта в системе. Поле является суррогатным первичным ключем таблицы.',
  `number` int(11) NOT NULL COMMENT 'Номер рецепта.',
  `validity_date` date NOT NULL COMMENT 'Дата, по истечении которой рецепт становится недействительным.',
  `client_id` int(11) NOT NULL COMMENT 'Внешний ключ, указывающий на клиента - владельца рецепта.',
  `doctor_id` int(11) NOT NULL DEFAULT '1' COMMENT 'Внешний ключ, указывающий на врача, выписавшего рецепт.',
  `status` enum('OPEN','CLOSED','EXPIRED') NOT NULL DEFAULT 'OPEN',
  PRIMARY KEY (`recipe_id`),
  KEY `fk_recipe_user1_idx` (`client_id`),
  KEY `fk_recipe_user2_idx` (`doctor_id`),
  CONSTRAINT `fk_recipe_user1` FOREIGN KEY (`client_id`) REFERENCES `user` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_recipe_user2` FOREIGN KEY (`doctor_id`) REFERENCES `user` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COMMENT='Таблица описывает электронный рецепт.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `recipe`
--

LOCK TABLES `recipe` WRITE;
/*!40000 ALTER TABLE `recipe` DISABLE KEYS */;
INSERT INTO `recipe` VALUES (1,2244,'2018-11-16',4,1,'CLOSED'),(2,5620,'2018-11-16',5,2,'OPEN'),(3,420,'2018-10-17',6,1,'OPEN'),(4,111,'2018-10-22',16,18,'OPEN'),(5,112,'2018-09-22',16,18,'EXPIRED'),(6,113,'2018-09-22',12,2,'EXPIRED'),(7,114,'2018-11-21',16,18,'OPEN'),(8,10,'2018-12-31',16,18,'OPEN');
/*!40000 ALTER TABLE `recipe` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `recipe_medicine_list`
--

DROP TABLE IF EXISTS `recipe_medicine_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `recipe_medicine_list` (
  `recipe_id` int(11) NOT NULL COMMENT 'Поле является частью составного первичного ключа и ссыалется на идентификатор рецепта.',
  `medicine_id` int(11) NOT NULL COMMENT 'Поле является частью составного первичного ключа и ссыалется на идентификатор лекарственного средства.',
  `amount` int(11) NOT NULL COMMENT 'Количество единиц лекарственного средства в рецепте.',
  `not_redeemed` int(11) DEFAULT NULL,
  PRIMARY KEY (`recipe_id`,`medicine_id`),
  KEY `fk_list_to_medicine_idx` (`medicine_id`),
  KEY `fk_list_to_recipe_idx` (`recipe_id`),
  CONSTRAINT `fk_list_to_medicine` FOREIGN KEY (`medicine_id`) REFERENCES `medicine` (`medicine_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_list_to_recipe` FOREIGN KEY (`recipe_id`) REFERENCES `recipe` (`recipe_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Таблица содержит список лекарственных средств, включенных в рецепт. Позволяет избежать соотношения "многие ко многим" между таблицами recipe и  medicine. Содержит составной первичный ключ (recipe_id + medicine_id), позволяющий однозначно идентифицировать запись в таблице.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `recipe_medicine_list`
--

LOCK TABLES `recipe_medicine_list` WRITE;
/*!40000 ALTER TABLE `recipe_medicine_list` DISABLE KEYS */;
INSERT INTO `recipe_medicine_list` VALUES (1,16,1,0),(2,14,1,1),(2,17,1,1),(2,18,1,1),(3,14,2,2),(4,14,4,3),(5,17,5,5),(7,14,4,4),(8,17,3,2),(8,18,2,2);
/*!40000 ALTER TABLE `recipe_medicine_list` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `recipe_order_list`
--

DROP TABLE IF EXISTS `recipe_order_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `recipe_order_list` (
  `recipe_id` int(11) NOT NULL COMMENT 'Часть составного первичного ключа, указывает на конкретную запись в таблице recipe.',
  `order_id` int(11) NOT NULL COMMENT 'Часть составного первичного ключа, указывает на конкретную запись в таблице order.',
  PRIMARY KEY (`recipe_id`,`order_id`),
  KEY `fk_recipe_order_list_recipe1_idx` (`recipe_id`),
  KEY `fk_recipe_order_list_order1` (`order_id`),
  CONSTRAINT `fk_recipe_order_list_order1` FOREIGN KEY (`order_id`) REFERENCES `order` (`order_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_recipe_order_list_recipe1` FOREIGN KEY (`recipe_id`) REFERENCES `recipe` (`recipe_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Таблица содержит список заказов, содержащих лекарственные средства, отпускаемые по рецепту. Имеет  составной первичный ключ (order_id + recipe_id), позволяющий однозначно идентифицировать запись в таблице.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `recipe_order_list`
--

LOCK TABLES `recipe_order_list` WRITE;
/*!40000 ALTER TABLE `recipe_order_list` DISABLE KEYS */;
INSERT INTO `recipe_order_list` VALUES (1,5),(2,6),(3,7);
/*!40000 ALTER TABLE `recipe_order_list` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Суррогатный первичный ключ, уникальный идентификатор пользователя в системе.',
  `login` varchar(25) NOT NULL COMMENT 'Логин пользователя для входа в систему.',
  `password` varchar(25) NOT NULL COMMENT 'Пароль пользователя для входа в систему.',
  `role` enum('CLIENT','PHARMACIST','DOCTOR') NOT NULL COMMENT 'Роль пользователя в системе, может соответствовать одному из значений: ''CLIENT'', ''PHARMACIST'', ''DOCTOR''.',
  `name` varchar(25) NOT NULL COMMENT 'Имя пользователя.',
  `surrname` varchar(25) NOT NULL COMMENT 'Фамилия пользователя.',
  `phone` varchar(20) NOT NULL COMMENT 'Телефон пользователя',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8 COMMENT='Таблица описывает пользователя системы.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'doctor_komarovsky','123komar','DOCTOR','Evgeny','Komarovsky','375296035999'),(2,'zhukov_doc','123456','DOCTOR','Vasyly','Zhukov','375296033449'),(3,'maria_petrova','petrova81','DOCTOR','Maria','Petrova','375446035999'),(4,'elena_ivanova','542894lena','PHARMACIST','Elena','Ivanova','375296635999'),(5,'shevchuk','shevchuk80','PHARMACIST','Stanislav','Shevchuk','375449603599'),(6,'lutik','qwert','CLIENT','Masha','Lycinskaya','375296036799'),(7,'zhuk','123zhuk456','CLIENT','Peter','Zhukov','375296035889'),(8,'Marusia','piatakova10','CLIENT','Maria','Piatakova','375442935999'),(9,'irisy','davidko','CLIENT','Irina','Kulik','375296035876'),(10,'victoria.zholudeva','vikasuper','CLIENT','victoria','zholudeva','375298765999'),(11,'lastik','qwerty','CLIENT','Victor','Lastov','375296035091'),(12,'Zhukovets','zhuk56','CLIENT','Sergey','Zhukovets','375296035007'),(13,'burik','buranov40','CLIENT','Igar','Buranov','375296045762'),(14,'ira_mama','kgggj','CLIENT','Irina','Vlasova','375443876111'),(15,'Jann-sodal','715445','CLIENT','Jan','Sodal','375299996035'),(16,'test','111','CLIENT','Masha','Lycinskaya','+375(29)603-67-99'),(17,'phar','222','PHARMACIST','Victor','Ivanov','+375(29)666-88-00'),(18,'doc','333','DOCTOR','Ivan','Fedorov','+375(33)666-88-00'),(19,'hhllllllllll','111','CLIENT','jhhhhhhhhll','jhhlhhlj','+375(29)600-60-60'),(20,'eee','rrr','CLIENT','qqq','www','+345(99)666-66-66'),(21,'zzz','xxx','CLIENT','uuu','ooo','+375(29)555-44-44'),(22,'login1','111','CLIENT','name','surname','+375(29)999-99-99'),(23,'zzzxxx','111','CLIENT','qqq','qqq','+375(29)600-00-00');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-10-18 22:36:41
